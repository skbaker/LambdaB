# Copy data nTuples, removing Lambda_b signal mass window, to leave 'blind'
# nTuples
#
# If PyROOT will not run, go into an LHCb environment to set it up
# e.g. $ SetupProject Urania v5r0
#
# Run with magnet orientation argument: MagUp = mu, MagDown = md
# i.e. $ python CutMassWindow.py mu
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Command line argument input
import sys
#print sys.argv[0] # prints CutMassWindow.py
#print sys.argv[1] # prints argument

from ROOT import *

# Run ROOT in batch mode
gROOT.SetBatch(True)

# Tuple covering full mass range
# Use same file for both Lb_dp and Lb_dppipi -- different ntuples
FullTupleFile = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip26/Data/Lb_dp_S26_2016_{}.root'.format(sys.argv[1])

original = TFile(FullTupleFile)
oldTree = original.Get('Lb_dppipi_Tuple/DecayTree')

# New file for cut tree
# Output to Lb_dppipi directory
ClonedTupleFile = '/eos/lhcb/user/s/skbaker/Lb_dppipi_Strip26/Data/{}/BLIND_Lb_dppipi_S26_2016_{}.root'.format(sys.argv[1],sys.argv[1])

run = 180861 # Run number where deuteron hyp was initialised
lower = 5470 # Mass window lower edge
upper = 5770 # Upper edge
CutString = 'runNumber > 180860 && (Lambda_b_M_deuthyp < {} || Lambda_b_M_deuthyp > {})'.format(lower,upper)

cloned = TFile(ClonedTupleFile,'recreate')
newTree = oldTree.CopyTree(CutString)
 
cloned.Write()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
