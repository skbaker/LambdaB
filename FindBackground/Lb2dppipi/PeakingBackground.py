#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Script to see if it is likely that there are any peaking backgrounds in the
# Lb -> dp signal region i.e. if either d or p is misidentified.
#
# Swap the mass hypotheses of the d and/or p for other particles, and calculate
# their new invariant mass. If peaks appear, then these could be contributing
# to the signal background.
#
# Something odd in the peaking bkg -- not removing the peak when trying to 
# get rid of the Lc(pKpi) mass!
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import string

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

#gROOT.SetBatch(True)

purple = TColor.GetColor("#5e3c99")
orange = TColor.GetColor("#e66101")

variables = ['Lambda_b_P'
            ,'Lambda_b_M_deuthyp'
            ,'p_ID'
            ,'pi_plus_ID'
            ,'pi_minus_ID'
            ,'p_PE'
            ,'p_P'
            ,'p_PX'
            ,'p_PY'
            ,'p_PZ'
            ,'deut_PE'
            ,'deut_P'
            ,'deut_PX'
            ,'deut_PY'
            ,'deut_PZ'
            ,'pi_plus_PE'
            ,'pi_plus_P'
            ,'pi_plus_PX'
            ,'pi_plus_PY'
            ,'pi_plus_PZ'
            ,'pi_minus_PE'
            ,'pi_minus_P'
            ,'pi_minus_PX'
            ,'pi_minus_PY'
            ,'pi_minus_PZ'
            #,'p_TRUEID' # Only for MC
            #,'deut_TRUEID'
            #,'pi_plus_TRUEID'
            ]

FileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip26/Data/'\
           'Lb_dp_S26_2016_mu.root'
           #'Lb_dp_S26_2016_md.root'

## MC file used for cross-checking of method
#FileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/'\
#           'MC_Lb_dp_2016_MagDown.root'


File = TFile(FileName)
tree = File.Get('Lb_dppipi_Tuple/DecayTree')
tree.SetBranchStatus('*',0)
for var in variables:
    tree.SetBranchStatus(var,1)

print tree.GetEntries()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Function to substitute masses for decay products
## A, B, C, D = masses for (d, p, pi+, pi-)
def SubMasses(A, B, C, D):
    #M2 = E2 - P2
    SubString = 'sqrt(((sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pip*M_pip + pi_plus_P*pi_plus_P) + '\
                'sqrt(M_pim*M_pim + pi_minus_P*pi_minus_P)) *'\
                '(sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pip*M_pip + pi_plus_P*pi_plus_P) + '\
                'sqrt(M_pim*M_pim + pi_minus_P*pi_minus_P))) -'\
                '(((deut_PX+p_PX+pi_plus_PX+pi_minus_PX)*'\
                '(deut_PX+p_PX+pi_plus_PX+pi_minus_PX)) + '\
                '((deut_PY+p_PY+pi_plus_PY+pi_minus_PY)*'\
                '(deut_PY+p_PY+pi_plus_PY+pi_minus_PY)) + '\
                '((deut_PZ+p_PZ+pi_plus_PZ+pi_minus_PZ)*'\
                '(deut_PZ+p_PZ+pi_plus_PZ+pi_minus_PZ))))'
    NewString = string.replace(SubString, 'M_d', str(A))
    NewString = string.replace(NewString, 'M_P', str(B))
    NewString = string.replace(NewString, 'M_pip', str(C))
    NewString = string.replace(NewString, 'M_pim', str(D))
    return NewString
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## If proton, use pi_plus; if antiproton, use pi_minus
## Separate strings for the two
def Sub3MassesPlus(A, B, C):
    #M2 = E2 - P2
    SubString = 'sqrt(((sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_plus_P*pi_plus_P)) *'\
                '(sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_plus_P*pi_plus_P))) -'\
                '(((deut_PX + p_PX + pi_plus_PX)*'\
                '(deut_PX + p_PX + pi_plus_PX)) + '\
                '((deut_PY + p_PY + pi_plus_PY)*'\
                '(deut_PY + p_PY + pi_plus_PY)) + '\
                '((deut_PZ + p_PZ + pi_plus_PZ)*'\
                '(deut_PZ + p_PZ + pi_plus_PZ))))'
    NewString = string.replace(SubString, 'M_d', str(A))
    NewString = string.replace(NewString, 'M_P', str(B))
    NewString = string.replace(NewString, 'M_pi', str(C))
    return NewString


def Sub3MassesMinus(A, B, C):
    #M2 = E2 - P2
    SubString = 'sqrt(((sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_minus_P*pi_minus_P)) *'\
                '(sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_minus_P*pi_minus_P))) -'\
                '(((deut_PX + p_PX + pi_minus_PX)*'\
                '(deut_PX + p_PX + pi_minus_PX)) + '\
                '((deut_PY + p_PY + pi_minus_PY)*'\
                '(deut_PY + p_PY + pi_minus_PY)) + '\
                '((deut_PZ + p_PZ + pi_minus_PZ)*'\
                '(deut_PZ + p_PZ + pi_minus_PZ))))'
    NewString = string.replace(SubString, 'M_d', str(A))
    NewString = string.replace(NewString, 'M_P', str(B))
    NewString = string.replace(NewString, 'M_pi', str(C))
    return NewString
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Particle masses:
M_deut = 1875.613
M_prot =  938.272
M_kaon =  493.677
M_pion =  139.570
M_muon =  105.658

## String to cut on Lc(pKpi) mass
threebodyplus = Sub3MassesPlus(M_prot,M_kaon,M_pion)
threebodyminus = Sub3MassesMinus(M_prot,M_kaon,M_pion)
## These cuts do not work because in the nTuples the pi_minus and pi_plus both
## can have both charges. The charge needs to match that off the proton in the
## decay, so rewrite selection to do that
## ---> 11/12/17, now should be correct, selecting the matching pion to proton
cutonmassplus  = '(((p_ID > 0 && pi_plus_ID > 0) && ('+threebodyplus+' > 2305 ||'\
            +threebodyplus+'< 2265))'\
            '|| ((p_ID < 0 && pi_plus_ID < 0) && ('+threebodyplus+' > 2305 ||'\
            +threebodyplus+'< 2265)))'
 

cutonmassminus = '(((p_ID > 0 && pi_minus_ID > 0) && ('+threebodyminus+' > 2305 ||'\
            +threebodyminus+'< 2265))'\
            '|| ((p_ID < 0 && pi_minus_ID < 0) && ('+threebodyminus+' > 2305 ||'\
            +threebodyminus+'< 2265)))' 

#cutonmass= '(p_ID>0 && (('+threebodyplus+')<2265 || ('+threebodyplus+')>2305))'\
#      ' || (p_ID<0 && (('+threebodyminus+')<2265 || ('+threebodyminus+')>2305))'
cutonmass = '('+cutonmassplus+') || ('+cutonmassminus+')'

CutLcMass = '(((p_ID > 0 && pi_plus_ID > 0) && ('+threebodyplus+' > 2305 ||'\
            +threebodyplus+'< 2265))'\
            '|| ((p_ID < 0 && pi_plus_ID < 0) && ('+threebodyplus+' > 2305 ||'\
            +threebodyplus+'< 2265))'\
            '|| ((p_ID > 0 && pi_minus_ID > 0) && ('+threebodyminus+' > 2305 ||'\
            +threebodyminus+'< 2265))'\
            '|| ((p_ID < 0 && pi_minus_ID < 0) && ('+threebodyminus+' > 2305 ||'\
            +threebodyminus+'< 2265)))' 

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Mass substitutions
straight = SubMasses(M_deut,M_prot,M_pion,M_pion)
d_to_p   = SubMasses(M_prot,M_prot,M_pion,M_pion)
d_to_K   = SubMasses(M_kaon,M_prot,M_pion,M_pion)
p_to_K   = SubMasses(M_deut,M_kaon,M_pion,M_pion)
pipitoKK  = SubMasses(M_deut,M_prot,M_kaon,M_kaon)
pi_to_K  = SubMasses(M_deut,M_prot,M_pion,M_kaon)
dtop_ptoK_pipitomumu = SubMasses(M_prot,M_kaon,M_muon,M_muon)
dtoK_pitomu = SubMasses(M_kaon,M_prot,M_pion,M_muon)
dtop_ptoK = SubMasses(M_prot,M_kaon,M_pion,M_pion)
dtop_ptoK_pitomu = SubMasses(M_kaon,M_prot,M_pion,M_muon)

hypotheses = [straight, d_to_p, d_to_K, p_to_K, pi_to_K, pipitoKK,\
              dtop_ptoK_pipitomumu, dtop_ptoK, dtop_ptoK_pitomu, dtoK_pitomu]
#hypotheses = []
names = ['straight', 'd_to_p', 'd_to_K','p_to_K', 'pi_to_K', 'pipitoKK',\
         'dtop_ptoK_pipitomumu','dtop_ptoK', 'dtop_ptoK_pitomu', 'dtoK_pitomu']

## Loop through hypotheses and names
for counter, hypothesis in enumerate(hypotheses):

    cv = TCanvas()
    cv.SetTicks()
    hist = TH1F('hist','hist',500,2000,7500)
    
    hist.GetXaxis().SetTitle('#Lambda_{b}\' [MeV]')
    hist.SetFillColor(purple)
    hist.SetFillStyle(3008)
    hist.SetLineColor(purple)
    
    tree.Draw(hypothesis+'>>hist')
    
    hist.Draw()
   
    leg = TLegend(0.15,0.75,0.4,0.85)
    leg.SetHeader(names[counter])
    leg.SetTextFont(1)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.Draw()

    cv.SaveAs('MassDist_'+names[counter]+'_MagUp.png')
    
    #raw_input("Press ENTER to continue...")
    del hist
    del cv
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Cut on the 3-body invariant mass
    if(hypothesis == d_to_K) or (hypothesis == dtop_ptoK_pitomu) \
                             or (hypothesis == dtoK_pitomu):
        cv = TCanvas()
        cv.SetTicks()
        hist = TH1F('hist','hist',500,2000,7500)
        
        #hist.GetXaxis().SetTitle('#Lambda_{b}\' [MeV]')
        hist.GetXaxis().SetTitle('M_{pK#pi} [MeV]')
        hist.SetFillColor(purple)
        hist.SetFillStyle(3008)
        hist.SetLineColor(purple)
        
        MassLcDaughters = Sub3MassesPlus(M_kaon,M_prot,M_pion)
        if(hypothesis==dtop_ptoK_pitomu): 
            MassLcDaughters = Sub3MassesPlus(M_prot,M_kaon,M_pion)

        # Cut on the mass of the Lc -> pKpi
        #tree.Draw(hypothesis+'>>hist',cutonmass)
        tree.Draw(hypothesis+'>>hist',cutonmassplus)
        tree.Draw(hypothesis+'>>hist',cutonmassminus)
        ## Draw threebody invariant mass
        #tree.Draw(threebodyminus+'>>hist',cutonmassminus)
        #tree.Draw(threebodyplus+'>>hist',cutonmassplus)
        hist.Draw()
      
        leg = TLegend(0.15,0.75,0.4,0.85)
        leg.SetHeader(names[counter]+', Lc cut')
        leg.SetTextFont(1)
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)
        leg.Draw()
 
        cv.SaveAs('MassDist_'+names[counter]+'_LcRemoved_MagUp.png')
        
        raw_input("Press ENTER to continue...")
        del hist
        del cv


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## How much signal is removed if you cut on the pKpi about the Lambda_c mass?
## M_Lc = 2286.48 MeV 
## Mass window in ANA Note: [2265, 2305]
##
## Remove this window from the signal

## MC file -- use pure, MC Truth selected MC
MCFileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/'\
             'Pure_MC_Lb_dppipi_2016_MagDown.root'

MCFile = TFile(MCFileName)
MCtree = MCFile.Get('DecayTree')
MCtree.SetBranchStatus('*',0)
for var in variables:
    MCtree.SetBranchStatus(var,1)

# Invariant mass of three particles, projected onto dppi+
threebodyplus = Sub3MassesPlus(M_deut,M_prot,M_pion)

cv = TCanvas()
h1 = TH1F('h1','h1',50,1000,6000)
h1.SetFillColor(purple)
h1.SetLineColor(purple)
h1.SetFillStyle(3020)
h1.GetXaxis().SetTitle('M(Kppi+) [MeV]')
h1.GetYaxis().SetTitle('Truth particles /100[MeV]')
MCtree.Draw(threebodyplus+'>>h1','p_ID>0')
MCtree.Draw(threebodyminus+'>>h1','p_ID<0')

h2 = TH1F('h2','h2',50,1000,6000)
h2.SetFillColor(orange)
h2.SetLineColor(orange)
h2.SetFillStyle(3008)
MCtree.Draw(threebodyplus+'>>h2',cutonmassplus)
MCtree.Draw(threebodyminus+'>>h2',cutonmassminus)

h1.Draw()
h2.Draw('same')

leg = TLegend(0.15,0.75,0.4,0.85)
leg.SetBorderSize(0)
leg.SetTextFont(1)
leg.AddEntry(h1,'All, '+str(int(h1.GetEntries())),'f')
leg.AddEntry(h2,'Lc removed, '+str(int(h2.GetEntries())),'f')
leg.Draw()

cv.SaveAs("Remove_LcMass_Kppi.png")

raw_input("Press ENTER to continue...")
"""
print 'Is it actually cutting?'
print 'DATA:'
print tree.GetEntriesFast()
print tree.GetEntries(cutonmass)
print 'MC:'
print MCtree.GetEntriesFast()
print MCtree.GetEntries(cutonmass)
"""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Plot sideband mass distribution
"""
c1 = TCanvas()
h3 = TH1F('h3','h3',220,4900,7100)
h3.GetXaxis().SetTitle('M_{#Lambda_{b}}')
h3.SetFillColor(orange)
h3.SetFillStyle(3020)
h3.SetLineColor(orange)

tree.Draw(straight+'>>h3',straight+'< 5470 ||'\
          +straight+' > 5770')
h3.Draw()
c1.SaveAs('Lb_dppipi_Data_BlindMass.png')

raw_input("Press ENTER to continue...")
"""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
