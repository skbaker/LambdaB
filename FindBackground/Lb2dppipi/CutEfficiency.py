# What is the efficiency of Lambda_b mass window cuts?
# Want 99% of signal to fall within window
#
# Use 'Pure' nTuples for this, which only contain MCTruth signal candidates
#
# If PyROOT will not run, go into an LHCb environment to set it up
# e.g. $ SetupProject Urania v5r0
#
# Run with magnet orientation argument: MagUp, MagDown
# i.e. $ python CutEfficiency.py MagDown 
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Command line argument input
import sys
#print sys.argv[0] # prints CutEfficiency.py
#print sys.argv[1] # prints argument

from ROOT import *

# Run ROOT in batch mode
gROOT.SetBatch(True)

# Tuple covering full mass range
FullTupleFile = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/'\
                'Pure_MC_Lb_dppipi_2016_{}.root'.format(sys.argv[1])

original = TFile(FullTupleFile)
oldTree = original.Get('DecayTree')

lower = 5470
upper = 5770 
CutString = 'Lambda_b_M_deuthyp < {} || Lambda_b_M_deuthyp > {}'.format(lower,upper)

before = oldTree.GetEntriesFast()
after = oldTree.GetEntries(CutString)

PrintString = 'Lower and upper Lambda_b mass cuts = {}, {}'.format(lower,upper)

print PrintString
print 'Cut efficiency = {}'.format(float(after)/float(before))
      
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
