# Find background

Scripts to cut the signal window from the Lb -> dppipi data.  
After this, all actions relating to Lb -> dppipi should be done in the same
directory as for Lb -> dp.

### CutEfficiency.py
Script to find the mass window to cut on.  
Want to remove 99% of the signal.  
Find that cut at 5470 < M\_Lb < 5770 achieves this.  
Run with magnet orientation as argument  
e.g. `python CutEfficiency.py MagUp`

### CutMassWindow.py
Script to remove the Lambda\_b mass window in data before fitting the background  
Run with magnet orientation as argument  
e.g. `python CutMassWindow.py mu`

### SubmitToBatch\_CutMass.sh
Cutting window from nTuples takes a long time, so better to submit it to batch.  
Run with `bsub -q 1nd < SubmitToBatch_CutMass.sh` on lxplus.  
Check that it is running - sometimes the LHCb environment doesn't configure.

### PeakingBackground.py
Script to find invariant mass of decay products when swapping different particle
hypotheses.  
**Trying** to get rid of Lc(pKpi) mass, but peaks aren't disappearing -- what is
going on??

![](MassDist_straight_MagUp.png)
![](MassDist_d_to_p_MagUp.png)
![](MassDist_p_to_K_MagUp.png)
![](MassDist_pi_to_K_MagUp.png)
![](MassDist_pipitoKK_MagUp.png)
![](MassDist_d_to_K_MagUp.png)
![](MassDist_dtop_ptoK_pipitomumu_MagUp.png)
![](MassDist_dtop_ptoK_pitomu_MagUp.png)
![](MassDist_dtoK_pitomu_MagUp.png)
![](MassDist_dtop_ptoK_MagUp.png)
![](MassDist_d_to_K_LcRemoved_MagUp.png)
![](MassDist_dtoK_pitomu_LcRemoved_MagUp.png)
![](MassDist_dtop_ptoK_pitomu_LcRemoved_MagUp.png)
MC Truth matched particles with Lc resonance removed.
![](Remove_LcMass_dppi.png)
![](Remove_LcMass_Kppi.png)
