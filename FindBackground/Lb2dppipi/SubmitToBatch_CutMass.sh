# Script to submit cutting of MD tree to batch, because it takes a long time
# locally

#!/bin/bash

cd /afs/cern.ch/user/s/skbaker/LambdaB/FindBackground/Lb2dppipi/
SetupProject Urania v5r0
python CutMassWindow.py mu
