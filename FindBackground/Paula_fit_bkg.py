# From Paula
# On lx00: /home/hep/palvare1/public/for_Blaise/fit_bkg.py

from ROOT import *

fin = TFile('/vols/lhcb/palvare1/B2pphgamma/BDT/training_samples.root')
t_bkg = fin.Get("BkgTree")
t_bkg.SetBranchStatus("*",0)
t_bkg.SetBranchStatus("B_plus_M",1)

## building the model
mass = RooRealVar("B_plus_M","B_plus_M",5100,7000)

c_exp = RooRealVar("c_exp","c_exp",-0.01,0)
model = RooExponential("BkgPDF","BkgPDF",mass,c_exp)

## getting the data
data = RooDataSet("data","data",t_bkg,RooArgSet(mass))


## fitting
mass.setRange("sideband",6000,6200)
fitres = model.fitTo(data,RooFit.Range("sideband"),RooFit.Save(1))


## plotting
cv = TCanvas()
fr = mass.frame()
data.plotOn(fr)
model.plotOn(fr)
model.plotOn(fr,RooFit.Range("full"),RooFit.LineStyle(kDashed))
fr.Draw()

mass.setRange("signal",5280-20,5280+20)
nfit = data.sumEntries("B_plus_M>6000 && B_plus_M<6200")
ntot = nfit/model.createIntegral(RooArgSet(mass),RooArgSet(mass),"sideband").getVal()
nsig = ntot*model.createIntegral(RooArgSet(mass),RooArgSet(mass),"signal").getVal()

print "The number of bkg events in the signal region is: ",nsig 
