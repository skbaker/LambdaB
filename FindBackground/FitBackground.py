# Find the level of background in the Lambda_b signal region
# 
# Might be best to cut the nTuple so that the Lambda_b mass window is not
# included at all. Want it to be efficient to cut ~ 95% of signal. The more
# efficient the better.
#
# If PyROOT will not run on lxplus, go into an LHCb environment
# e.g. SetupProject Urania v5r0
#
# Run with tuple file argument
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import sys
#print sys.argv[0] # prints FitBackground.py
#print sys.argv[0] # prints argument

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

## Set sideband and signal limits
low_upperMassSideband = 5770
top_upperMassSideband = 7000
low_MassWindow = 5470 
low_lowerMassSideband = 5000 

## Path for plots
PlotsPath = '/afs/cern.ch/user/s/skbaker/LambdaB/FindBackground/Plots/'

## Run ROOT in batch mode
#gROOT.SetBatch(True)

## Read in ROOT nTuple from command line
TupleFileName = sys.argv[1]

fin = TFile(TupleFileName)
t_bkg = fin.Get("DecayTree")
t_bkg.SetBranchStatus("*",0)
t_bkg.SetBranchStatus("Lambda_b_M_deuthyp",1)

## Building the model
# Only want to look at upper sideband, so lower limit should be lower limit
# of upper mass sideband
mass = RooRealVar('Lambda_b_M_deuthyp','Lambda_b_M_deuthyp',
                  low_lowerMassSideband,top_upperMassSideband)

# This is the variable that you are looking to fit
c_exp = RooRealVar("c_exp","c_exp",-0.01,0)
# Probably just want to use an exponential
model = RooExponential("BkgPDF","BkgPDF",mass,c_exp)

## getting the data
data = RooDataSet("data","data",t_bkg,RooArgSet(mass))

## fitting
# Don't need to remove this central window, if the tree was cut already
mass.setRange("sideband",low_upperMassSideband,top_upperMassSideband)
fitres = model.fitTo(data,RooFit.Range("sideband"),RooFit.Save(1))

## plotting
cv = TCanvas()
fr = mass.frame()
data.plotOn(fr)
model.plotOn(fr)
model.plotOn(fr,RooFit.Range("full"),RooFit.LineStyle(kDashed))
fr.Draw()

## Create name for plot
Pieces = TupleFileName.split("/")
FrameFileName = Pieces[(len(Pieces)-1)]
FrameFileName = FrameFileName.replace('.root','_background.png')
FrameFilePath = PlotsPath+FrameFileName
print FrameFilePath
cv.SaveAs(FrameFilePath)

mass.setRange("signal",low_MassWindow,low_upperMassSideband)
SidebandCut = 'Lambda_b_M_deuthyp>{} && Lambda_b_M_deuthyp<{}'.format(low_upperMassSideband,top_upperMassSideband)
nfit = data.sumEntries(SidebandCut)
ntot = nfit/model.createIntegral(RooArgSet(mass),RooArgSet(mass),"sideband").getVal()
nsig = ntot*model.createIntegral(RooArgSet(mass),RooArgSet(mass),"signal").getVal()

print "nfit: ",nfit
print "Number of entries in sideband: ",ntot
print "The number of bkg events in the signal region is: ",nsig 

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
