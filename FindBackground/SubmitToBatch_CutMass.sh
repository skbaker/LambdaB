# Script to submit cutting of MD tree to batch, because it takes a long time
# locally

cd /afs/cern.ch/user/s/skbaker/LambdaB/FindBackground/
SetupProject Urania v5r0
python CutMassWindow.py md
