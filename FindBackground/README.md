# Find background

Scripts to find the level of background in the signal region.  

### Paula\_fit\_bkg.py
Script from Paula

### DataTuples.py
List of blinded data nTuples.

### RunScripts.sh
Script to run the FitBackground.py script on each of the blinded datasets, for
magnet orientations, and Lb -> dp, Lb -> dppipi nTuples.  
Run as `source RunScripts.sh`  
Plots will be output in /Plots/ folder.

### FitBackground.py
Script to fit my Lambda\_b background  
Run with blinded data nTuple as argument  
e.g. `python FitBackground.py nTuple.root`

### CutEfficiency.py
Script to find the mass window to cut on.  
Want to remove 99% of the signal.  
Find that cut at 5470 < M\_Lb < 5770 achieves this.  
Run with magnet orientation as argument  
e.g. `python CutEfficiency.py MagUp`

### CutMassWindow.py
Script to remove the Lambda\_b mass window in data before fitting the background  
Run with magnet orientation as argument  
e.g. `python CutMassWindow.py mu`

### PeakingBackground.py
Script to swap mass hypothesis of decay products, to see if any peaking
backgrounds emerge.  
i.e. could get a peak at ~ 3 GeV for J/Psi -> pp

![](MassDist_straight_MagUp.png)
![](MassDist_d_to_K_MagUp.png)
![](MassDist_d_to_K_p_to_pi_MagUp.png)
![](MassDist_d_to_K_p_to_K_MagUp.png)
![](MassDist_d_to_p_MagUp.png)
![](MassDist_d_to_p_p_to_K_MagUp.png)
![](MassDist_d_to_p_p_to_pi_MagUp.png)
