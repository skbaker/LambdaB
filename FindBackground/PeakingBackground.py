#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Script to see if it is likely that there are any peaking backgrounds in the
# Lb -> dp signal region i.e. if either d or p is misidentified.
#
# Swap the mass hypotheses of the d and/or p for other particles, and calculate
# their new invariant mass. If peaks appear, then these could be contributing
# to the signal background.
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import string

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

#gROOT.SetBatch(True)

purple = TColor.GetColor("#5e3c99")
orange = TColor.GetColor("#e66101")

variables = ['Lambda_b_P'
            ,'Lambda_b_M'
            ,'p_PE'
            ,'p_P'
            ,'p_PX'
            ,'p_PY'
            ,'p_PZ'
            ,'deut_PE'
            ,'deut_P'
            ,'deut_PX'
            ,'deut_PY'
            ,'deut_PZ'
            #,'p_TRUEID' # Only for MC
            #,'deut_TRUEID'
            ]

FileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip26/Data/'\
           'Lb_dp_S26_2016_mu.root'
           #'Lb_dp_S26_2016_md.root'

## MC file used for cross-checking of method
#FileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/'\
#           'MC_Lb_dp_2016_MagDown.root'


File = TFile(FileName)
tree = File.Get('Lb_dp_Tuple/DecayTree')
tree.SetBranchStatus('*',0)
for var in variables:
    tree.SetBranchStatus(var,1)

print tree.GetEntries()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Fucntion to substitute masses for decay products
## A, B = masses for (d, p)
def SubMasses(A, B):
    #M2 = E2 - P2
    SubString = 'sqrt(((sqrt(M_d*M_d + deut_P*deut_P) + '\
                'sqrt(M_p*M_p + p_P*p_P))*'\
                '(sqrt(M_d*M_d + deut_P*deut_P) + '\
                'sqrt(M_p*M_p + p_P*p_P))) - '\
                '(((deut_PX+p_PX)*(deut_PX+p_PX)) + '\
                '((deut_PY+p_PY)*(deut_PY+p_PY)) + '\
                '((deut_PZ+p_PZ)*(deut_PZ+p_PZ))))'
    NewString = string.replace(SubString, 'M_d', str(A))
    NewString = string.replace(NewString, 'M_p', str(B))
    return NewString

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Particle masses
M_deut = 1875.613
M_prot =  938.272
M_kaon =  483.677
M_pion =  139.570

straight = SubMasses(M_deut, M_prot)
d_to_p   = SubMasses(M_prot, M_prot)
d_to_p_p_to_K = SubMasses(M_prot, M_kaon)
d_to_K = SubMasses(M_kaon, M_prot)
d_to_K_p_to_pi = SubMasses(M_kaon, M_pion)
d_to_p_p_to_pi = SubMasses(M_prot, M_pion)
d_to_K_p_to_K = SubMasses(M_kaon, M_kaon)

hypotheses = [straight, d_to_p, d_to_p_p_to_K, d_to_K, d_to_K_p_to_pi,\
              d_to_p_p_to_pi, d_to_K_p_to_K]
names = ['straight', 'd_to_p', 'd_to_p_p_to_K','d_to_K','d_to_K_p_to_pi',\
         'd_to_p_p_to_pi', 'd_to_K_p_to_K']

## Loop through hypotheses and names
for counter, hypothesis in enumerate(hypotheses):

    cv = TCanvas()
    cv.SetTicks()
    hist = TH1F('hist','hist',500,2000,7000)

    hist.GetXaxis().SetTitle('#Lambda_{b}\' [MeV]')
    hist.SetFillColor(purple)
    hist.SetFillStyle(3008)
    hist.SetLineColor(purple)

    tree.Draw(hypothesis+'>>hist')
    hist.Draw()

    leg = TLegend(0.15,0.8,0.45,0.85)
    leg.SetHeader(names[counter])
    leg.SetFillStyle(0)
    leg.SetTextFont(1)
    leg.SetBorderSize(0)
    leg.Draw()

    cv.SaveAs('MassDist_'+names[counter]+'_MagUp.png')

    #raw_input("Press ENTER to continue...")
    del hist
    del cv
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Plot sideband mass distribution

c1 = TCanvas()
h1 = TH1F('h1','h1',220,4900,7100)
h1.GetXaxis().SetTitle('M_{#Lambda_{b}}')
h1.SetFillColor(orange)
h1.SetFillStyle(3020)
h1.SetLineColor(orange)

tree.Draw(straight+'>>h1',straight+'< 5470 ||'\
          +straight+' > 5770')
h1.Draw()
c1.SaveAs('Lb_dp_Data_BlindMass.png')

raw_input("Press ENTER to continue...")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
