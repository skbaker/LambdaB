# Plots of background fit
Generated using `../FitBackground.py`  
Also displayed at http://www.hep.ph.ic.ac.uk/~sb2410/LambdaB/BackgroundFit/  

### Lb -> dp, MagDown
![](./BLIND_Lb_dp_S26_2016_md_background.png)
### Lb -> dp, MagUp
![](./BLIND_Lb_dp_S26_2016_mu_background.png)
### Lb -> dppipi, MagDown
![](./BLIND_Lb_dppipi_S26_2016_md_background.png)
### Lb -> dppipi, MagUp
![](./BLIND_Lb_dppipi_S26_2016_mu_background.png)
