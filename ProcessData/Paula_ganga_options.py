import subprocess

name = "MinBias_NoPIDPions"

DataTypes = ["2016"]
bkkpath = { "2016" : {'MD' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping26/90000000/MINIBIAS.DST',
                      'MU' : '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping26/90000000/MINIBIAS.DST'}
            }


myBackend = Dirac()
#myBackend = Local()

### Splitter
mySplitter = SplitByFiles()
mySplitter.filesPerJob = 2
mySplitter.maxFiles = -1
# mySplitter.ignoremissing = False
# mySplitter.bulksubmit = False

# ### Merger
# myMerger = RootMerger()
# myMerger.overwrite = True     #False by default
# myMerger.ignorefailed = True  #False by default


for DataType in DataTypes:
    
    print "Submiting ",DataType, " data..."    

    myJobName = name+"_"+DataType
    tuplefilename = name+"_"+DataType+".root"
    _myOutputdata =[tuplefilename]
    # myMerger.files = [tuplefilename]
    
    data = LHCbDataset()
    for mag in bkkpath[DataType].keys(): 
        path = bkkpath[DataType][mag]
        bkkquery = BKQuery(path,
                           dqflag=['OK'],
                           # startDate = "2016-07-01" ,
                           # selection = "Runs" ,
                           # endDate = "2016-12-31" ,
                           # type = "RunsByDate"
                           )
        data = bkkquery.getDataset()
        if not data:
            print "No data for ",DataType
            break
        

        # myApplication = DaVinci()
        myApplication = GaudiExec()
        # myApplication.version = "v38r1p1"

        ### Define job-options file
        myApplication.directory = "/home/hep/palvare1/LHCbDev/DaVinciDev_v41r2p1/"
        myApplication.options = ["/home/hep/palvare1/AntiDeuteron/Data/Inclusive/DaVinci-Job.py"]
        myApplication.extraOpts = "DaVinci().DataType = '%s' \n"%DataType
        myApplication.extraOpts += "DaVinci().TupleFile  = '%s' \n"%tuplefilename


        data = data[0:1]
        
        j = Job (
            name         = myJobName+"_"+mag,
            application  = myApplication,
            splitter     = mySplitter,
            # postprocessors       = myMerger,
            # inputfiles = _inputsandbox,
            outputfiles   = _myOutputdata,
            backend      = myBackend,
            inputdata    = data,
            )


        j.submit()
        # queues.add(j.submit)
        
        print "... ",mag," out..."

    print "... Done!"



