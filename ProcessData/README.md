# ProcessData

Directory for scripts to process the 2016 Lb2dp and Lb2dppipi data.  
Use Stripping28r1.

Was processed previously by Paula, but did not include the deuteron PID info.
`Paula_ganga_options.py` and `Paula_NTupleMaker_Lb2dp.py` scripts from Paula 
for previous processing attempt.

### ganga\_Data\_DaVinci.py
Script for Grid submission.  
Select data for runs after August 2016, [180861 - 185611].  
This will submit Ganga jobs for both magnet polarities - separate later if
necessary.

### DaVinci\_Lb\_dp\_Data\_selection\_2016.py
Options script for reprocessing data. Copied from Paula's script and script
used for MC.
Tuples should contain deuteron PID info.
Generates Lb2dp and Lb2dppipi nTuples in one file from one script.
