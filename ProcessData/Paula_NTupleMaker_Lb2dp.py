from Gaudi.Configuration import *


from Configurables import GaudiSequencer
from Configurables import LoKi__HDRFilter

###### Filter only my line
line = "Lb2dp_Line"
linepipi = "Lb2dp_pipiLine"
sf = LoKi__HDRFilter( 'StripPassFilter', Code="HLT_PASS('Stripping"+line+"Decision')", Location="/Event/Strip/Phys/DecReports" )
sf2 = LoKi__HDRFilter( 'StripPassFilter', Code="HLT_PASS('Stripping"+linepipi+"Decision')", Location="/Event/Strip/Phys/DecReports" )


MySequencer = GaudiSequencer('Sequence')
MySequencer.Members = [sf,sf2]


# Fill Tuple Lb2dp
#####################
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS
from Configurables import FilterDesktop, TupleToolStripping,  TupleToolMCTruth, TupleToolKinematic
from Configurables import TupleToolMCBackgroundInfo, MCTupleToolDecayType, MCTupleToolEventType #, MCTupleToolBremInfo
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats
from Configurables import TupleToolDecayTreeFitter
from Configurables import LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *


tuple = DecayTreeTuple("Lb_dp_Tuple")
tuple.Inputs = ['/Event/BhadronCompleteEvent/Phys/Lb2dp_Line/Particles']


tuple.Decay = "[Lambda_b0 -> ^K+ ^p~-]CC"



tuple.addBranches({
    "Lambda_b"      : "[Lambda_b0 -> K+ p~-]CC",
    "deut"      : "[Lambda_b0 -> ^K+ p~-]CC",
    "p" : "[Lambda_b0 -> K+ ^p~-]CC"
         })




tuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    ]



## Reference point
tuple.ToolList+=[ "TupleToolKinematic" ]
tuple.addTool(TupleToolKinematic, name="TupleToolKinematic")
tuple.TupleToolKinematic.Verbose = True

_trigger_list = [
    #L0
    'L0HadronDecision'
    ,'L0ElectronDecision'
    ,'L0ElectronHiDecision'
    ,'L0MuonDecision'
    ,'L0DiMuonDecision'
    ,'L0MuonHighDecision'
    ,'L0PhotonDecision'
    #Hlt1
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonNoIPDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1MultiMuonNoL0Decision'
    ,'Hlt1CalibMuonAlignJpsiDecision'
    ,'Hlt1DiMuonNoL0Decision'
    ,'Hlt1TrackMVADecision'
    ,'Hlt1TwoTrackMVADecision'
    ,'Hlt1B2GammaGammaDecision'
    ,'Hlt1B2HH_LTUNB_KKDecision'
    ,'Hlt1B2HH_LTUNB_KPiDecision'
    ,'Hlt1B2HH_LTUNB_PiPiDecision'
    ,'Hlt1B2PhiGamma_LTUNBDecision'
    ,'Hlt1B2PhiPhi_LTUNBDecision'
    ,'Hlt1BeamGasBeam1Decision'
    ,'Hlt1BeamGasBeam2Decision'
    ,'Hlt1BeamGasCrossingEnhancedBeam1Decision'
    ,'Hlt1BeamGasCrossingEnhancedBeam2Decision'
    ,'Hlt1BeamGasCrossingForcedRecoDecision'
    ,'Hlt1BeamGasCrossingForcedRecoFullZDecision'
    ,'Hlt1BeamGasHighRhoVerticesDecision'
    ,'Hlt1BeamGasNoBeamBeam1Decision'
    ,'Hlt1BeamGasNoBeamBeam2Decision'
    ,'Hlt1CEPDecision'
    ,'Hlt1CEPVeloCutDecision'
    ,'Hlt1CalibHighPTLowMultTrksDecision'
    ,'Hlt1CalibRICHMirrorRICH1Decision'
    ,'Hlt1CalibRICHMirrorRICH2Decision'
    ,'Hlt1CalibTrackingKKDecision'
    ,'Hlt1CalibTrackingKPiDecision'
    ,'Hlt1CalibTrackingKPiDetachedDecision'
    ,'Hlt1CalibTrackingPiPiDecision'
    #,'Hlt1DiProtonDecision'
    #,'Hlt1DiProtonLowMultDecision'
    ,'Hlt1IncPhiDecision'
    ,'Hlt1L0AnyDecision'
    ,'Hlt1L0AnyNoSPDDecision'
    ,'Hlt1LumiDecision'
    ,'Hlt1MBNoBiasDecision'
    ,'Hlt1MBNoBiasRateLimitedDecision'
    ,'Hlt1NoBiasNonBeamBeamDecision'
    ,'Hlt1NoPVPassThroughDecision'
    ,'Hlt1ODINTechnicalDecision'
    ,'Hlt1SingleElectronNoIPDecision'
    ,'Hlt1Tell1ErrorDecision'
    ,'Hlt1TrackMuonNoSPDDecision'
    ,'Hlt1VeloClosingMicroBiasDecision'
    ,'Hlt1BeamGasCrossingParasiticDecision'
    ,'Hlt1ErrorEventDecision'
    ,'Hlt1GlobalDecision'
    #Hlt2
    ,'Hlt2DebugEventDecision'
    ,'Hlt2DiMuonBDecision'
    ,'Hlt2DiMuonBTurboDecision'
    ,'Hlt2DiMuonBTurboDecision'
    ,'Hlt2DiMuonDetachedDecision'
    ,'Hlt2DiMuonDetachedHeavyDecision'
    ,'Hlt2DiMuonDetachedJPsiDecision'
    ,'Hlt2DiMuonDetachedPsi2SDecision'
    ,'Hlt2DiMuonJPsiDecision'
    ,'Hlt2DiMuonJPsiHighPTDecision'
    ,'Hlt2DiMuonJPsiTurboDecision'
    ,'Hlt2DiMuonJPsiTurboDecision'
    ,'Hlt2DiMuonPsi2SDecision'
    ,'Hlt2DiMuonPsi2SHighPTDecision'
    ,'Hlt2DiMuonPsi2STurboDecision'
    ,'Hlt2DiMuonPsi2STurboDecision'
    ,'Hlt2DiMuonSoftDecision'
    ,'Hlt2DiMuonZDecision'
    ,'Hlt2EWDiElectronDYDecision'
    ,'Hlt2EWDiElectronHighMassDecision'
    ,'Hlt2EWDiMuonDY1Decision'
    ,'Hlt2EWDiMuonDY2Decision'
    ,'Hlt2EWDiMuonDY3Decision'
    ,'Hlt2EWDiMuonDY4Decision'
    ,'Hlt2EWDiMuonZDecision'
    ,'Hlt2EWSingleElectronHighPtDecision'
    ,'Hlt2EWSingleElectronLowPtDecision'
    ,'Hlt2EWSingleElectronVHighPtDecision'
    ,'Hlt2EWSingleMuonHighPtDecision'
    ,'Hlt2EWSingleMuonLowPtDecision'
    ,'Hlt2EWSingleMuonVHighPtDecision'
    ,'Hlt2EWSingleTauHighPt2ProngDecision'
    ,'Hlt2EWSingleTauHighPt3ProngDecision'
    ,'Hlt2ForwardDecision'
    ,'Hlt2SingleMuonDecision'
    ,'Hlt2SingleMuonHighPTDecision'
    ,'Hlt2SingleMuonLowPTDecision'
    ,'Hlt2SingleMuonNoSPDDecision'
    ,'Hlt2SingleMuonRareDecision'
    ,'Hlt2SingleMuonVHighPTDecision'
    ,'Hlt2Topo2BodyDecision'
    ,'Hlt2Topo3BodyDecision'
    ,'Hlt2Topo4BodyDecision'
    ,'Hlt2TopoMu2BodyDecision'
    ,'Hlt2TopoMu3BodyDecision'
    ,'Hlt2TopoMu4BodyDecision'
    ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalibDecision'
    ,'Hlt2TransparentDecision'
    ,'Hlt2ErrorEventDecision'
    ,'Hlt2GlobalDecision'
    ]


tuple.ToolList+=[ "TupleToolTISTOS" ]
tuple.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
tuple.TupleToolTISTOS.Verbose = True
tuple.TupleToolTISTOS.VerboseL0 = True
tuple.TupleToolTISTOS.VerboseHlt1 = True
tuple.TupleToolTISTOS.VerboseHlt2 = True
tuple.TupleToolTISTOS.TriggerList = _trigger_list #trigger_list.trigger_list()


## Dmass variables
from Configurables import TupleToolSubMass
tuple.addTool(TupleToolSubMass,name="TupleToolSubMass")
tuple.TupleToolSubMass.SetMax = 3
tuple.TupleToolSubMass.Substitution += ["K+ => deuteron"]
# tuple.TupleToolSubMass.OutputLevel = DEBUG
tuple.ToolList += ["TupleToolSubMass"]



from Configurables import TupleToolRecoStats
tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
tuple.TupleToolRecoStats.Verbose=True

# Add eta
LoKi_All=tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA'
    }



# Fill Tuple Lb2dppipi
######################

tuplepipi = tuple.clone("Lb_dppipi_Tuple")
tuplepipi.Inputs = ['/Event/BhadronCompleteEvent/Phys/Lb2dp_pipiLine/Particles']


tuplepipi.Decay = "[Lambda_b0 -> ^K+ ^p~- ^pi+ ^pi-]CC"



tuplepipi.addBranches({
    "Lambda_b"      : "[Lambda_b0 -> K+ p~- pi+ pi-]CC",
    "deut"      : "[Lambda_b0 -> ^K+ p~- pi+ pi-]CC",
    "p" : "[Lambda_b0 -> K+ ^p~- pi+ pi-]CC",
    "pi_plus" : "[(Lambda_b0 -> K+ p~- ^pi+ pi-),(Lambda_b~0 -> K- p+ pi- ^pi+)]",
    "pi_minus" : "[(Lambda_b0 -> K+ p~- pi+ ^pi-),(Lambda_b~0 -> K- p+ ^pi- pi+)]"
         })


# Add mass under deuteron hypothesis
#####################################

Lb_loki = tuple.Lambda_b.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Lb")
Lb_loki.Preambulo = [ "from LoKiPhys.decorators import *",
                      "m_deut  = 1.875613*1000",
                      "P_deut = MINTREE(ABSID=='K+',P)",
                      "E_deut = sqrt(m_deut*m_deut + P_deut*P_deut)",
                      "Px_Lb   = (CHILD(PX,1)+CHILD(PX,2))",
                      "Py_Lb   = (CHILD(PY,1)+CHILD(PY,2))",
                      "Pz_Lb   = (CHILD(PZ,1)+CHILD(PZ,2))",
                      "P2_Lb = Px_Lb*Px_Lb + Py_Lb*Py_Lb + Pz_Lb*Pz_Lb",
                      "E_Lb = E_deut + MINTREE(ABSID=='p+',E)",
                      "M_Lb = sqrt(E_Lb*E_Lb - P2_Lb)"]

Lb_loki.Variables = {"M_deuthyp" : "M_Lb"}


Lb_loki_pipi = tuplepipi.Lambda_b.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Lb_pipi")
Lb_loki_pipi.Preambulo = [ "from LoKiPhys.decorators import *",
                       "m_deut  = 1.875613*1000",
                       "P_deut = MINTREE(ABSID=='K+',P)",
                       "E_deut = sqrt(m_deut*m_deut + P_deut*P_deut)",
                       "Px_Lb   = (CHILD(PX,1)+CHILD(PX,2)+CHILD(PX,3)+CHILD(PX,4))",
                       "Py_Lb   = (CHILD(PY,1)+CHILD(PY,2)+CHILD(PY,3)+CHILD(PY,4))",
                       "Pz_Lb   = (CHILD(PZ,1)+CHILD(PZ,2)+CHILD(PZ,3)+CHILD(PZ,4))",
                       "P2_Lb = Px_Lb*Px_Lb + Py_Lb*Py_Lb + Pz_Lb*Pz_Lb",
                       "E_Lb = E_deut + MINTREE(ABSID=='p+',E) + MINTREE(ID=='pi+',E) + MINTREE(ID=='pi-',E)",
                       "M_Lb = sqrt(E_Lb*E_Lb - P2_Lb)"]

Lb_loki_pipi.Variables = {"M_deuthyp" : "M_Lb"}


# Configure DaVinci
####################

from Configurables import DaVinci
DaVinci().HistogramFile = 'DV_filter_histos.root'
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
DaVinci().UserAlgorithms = [ MySequencer ]
DaVinci().UserAlgorithms += [tuple]
DaVinci().UserAlgorithms += [tuplepipi]
DaVinci().InputType = "DST"


# # database
# DaVinci().DDDBtag  = "dddb-20120831"
# DaVinci().CondDBtag = "cond-20121008"

# importOptions("/afs/cern.ch/user/a/acontu/public/S21Incremental_dsts/PFNBhadronCompleteEvent.dst.py")
# importOptions("/afs/cern.ch/user/a/alvarezc/AntiDeuteron/Stripping26/Validation/data/data_strip_validation_md.py")

# from GaudiConf import IOExtension, IOHelper
# IOExtension().inputFiles(["/afs/cern.ch/user/a/alvarezc/AntiDeuteron/Stripping26/Validation/data/00050713_00000012_1.bhadroncompleteevent.dst"])
