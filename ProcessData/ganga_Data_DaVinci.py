#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Run DaVinci on the grid
## Script used to process DST files for 2016 data into nTuples.
## Copied from same script for 2016 MC
## Run a Ganga job for both MagUp and MagDown, for runs after August only. 
## ROOT nTuples must be merged afterwards
##
## Name of ROOT nTuples needs to match name in DaVinci script
##
## Run as: $ ganga ganga_DaVinci.py
## 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Sumbmission functions copied from Matt:
def sendJob(filename, dataset, name):
    j = Job()
    j.application = DaVinci(version='v41r1')
    j.application.optsfile = [ filename ]
    j.backend = Dirac()
    j.inputdata = dataset
    j.splitter = SplitByFiles(filesPerJob=15)
    j.outputfiles = [ DiracFile('Data_Lb_dp_2016.root') ]
    j.parallel_submit = True
    j.name = name
    j.submit()

def getFromBookkeeping(datapath):
    bkk = BKQuery(datapath)
    data = bkk.getDataset()
    return data

def getFromBookkeeping_selectRuns(runpath):
    bkk = BKQuery(dqflag = "OK",
                  path = runpath,
                  type = "Run"
                 )
    data = bkk.getDataset()
    return data

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Try submitting job using Matt's method

## DSTs for submitting all data, but use method below for selected run numbers
"""
firstDataSetPath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/'\
            'Real Data/Reco16/Stripping28r1/90000000/BHADRONCOMPLETEEVENT.DST'
##firstDataSetPath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/'\
            'Real Data/Reco16/Stripping28r1/90000000/BHADRONCOMPLETEEVENT.DST'
Data = getFromBookkeeping(firstDataSetPath)
print Data
"""

## Select data based on run number from August 2016 onwards, [180861 - 185611]
## This will run for both polarities
runSelectionDataPath = '180861-185611/Real Data/Reco16/Stripping28r1/90000000/'\
                       'BHADRONCOMPLETEEVENT.DST'
Data = getFromBookkeeping_selectRuns(runSelectionDataPath)

## Modify so that Data is only one file, for trial
## REMOVE this line to submit the full dataset
#Data.files = Data.files[0:150]
#print Data

sendJob('DaVinci_Lb_dp_Data_selection_2016.py', Data, runSelectionDataPath)
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
