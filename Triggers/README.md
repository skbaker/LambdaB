# Find efficiencies of different triggers
Efficiencies of selected triggers on pure MC signal samples.  
Triggers chosen that are positive for signal, i.e. muon and other triggers
are useless for these decays.  
Plots displayed in subdirectory.  


Triggers:
* `Lambda_b_L0HadronDecision_TOS`
* `Lambda_b_L0Global_TIS`
* `Lambda_b_L0Global_Dec`
* `Lambda_b_Hlt1Global_Dec`
* `Lambda_b_Hlt1Global_TIS`
* `Lambda_b_Hlt1Global_TOS`
* `Lambda_b_Hlt1Phys_Dec`
* `Lambda_b_Hlt2ForwardDecsion_Dec`
* `Lambda_b_Hlt2Global_Dec`
* `Lambda_b_Hlt2Global_TIS`
* `Lambda_b_Hlt2Global_TOS`
* `Lambda_b_Hlt2Phys_Dec`
* `Lambda_b_Hlt2Phys_TIS`
* `Lambda_b_Hlt2Phys_TOS`


### TriggerEfficiencies.py
Finds and plots efficiencies of a few different triggers.

### CombineTriggers.py
Find efficiency of combinations of triggers.
Combine L0, HLT1 and HLT2 triggers to improve selection efficiencies.
Can gain a fair amount for L0, and on the percent level for others.

### RunScripts.sh
Runs `TriggerEfficiencies.py` and `CombineTriggers.py` over the pure MC signal nTuples.  
Run as `source RunScripts.sh`

