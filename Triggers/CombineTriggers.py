# Find efficiency of combined tiggers
# i.e. L0HadronDecision and/or L0Global
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from __future__ import division

import sys

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

PlotsPath = '/afs/cern.ch/user/s/skbaker/LambdaB/Triggers/Plots/'

TupleFileName = sys.argv[1]

names = [ 'Lambda_b_L0HadronDecision_TOS'
        , 'Lambda_b_L0Global_TIS'
        , 'Lambda_b_Hlt1Global_TIS'
        , 'Lambda_b_Hlt1Global_TOS'
        , 'Lambda_b_Hlt1Phys_Dec'
        , 'Lambda_b_Hlt2ForwardDecision_Dec'
        , 'Lambda_b_Hlt2Global_TIS'
        , 'Lambda_b_Hlt2Global_TOS'
        , 'Lambda_b_Hlt2Phys_TIS'
        , 'Lambda_b_Hlt2Phys_TOS' 
        ]

## List of trigger combinations
combos = [ 'Lambda_b_L0HadronDecision_TOS == 1 || Lambda_b_L0Global_TIS == 1'
         , 'Lambda_b_Hlt1Global_TIS == 1 || Lambda_b_Hlt1Global_TOS == 1 '\
           '|| Lambda_b_Hlt1Phys_Dec == 1 || Lambda_b_Hlt2ForwardDecision_Dec == 1'
         , 'Lambda_b_Hlt2Global_TIS == 1 || Lambda_b_Hlt2Global_TOS == 1'
         , 'Lambda_b_Hlt2Phys_TIS == 1 || Lambda_b_Hlt2Phys_TOS == 1'
        ]

fin = TFile(TupleFileName)
tree = fin.Get("DecayTree")
tree.SetBranchStatus("*",0)
for name in names:
    tree.SetBranchStatus(name,1)

## Total entries in tree
entries = tree.GetEntries()

h1 = TH1D("h1","Trigger efficiencies",len(combos),0,len(combos))
h1.SetMaximum(1.1)
h1.SetMinimum(0)
h1.SetFillStyle(1001)
h1.SetFillColor(30)
h1.SetLineColor(30)
h1.SetBarWidth(0.7)
h1.GetYaxis().SetTitle('Trigger efficiency')

efficiencies = []

## Entries passing each trigger
for i, cut in enumerate(combos):
    passed = tree.GetEntries(cut)
    fraction = passed/entries
    h1.GetXaxis().SetBinLabel((i+1),cut)
    h1.SetBinContent((i+1),fraction)
    efficiencies.append(fraction)
    print cut
    print fraction

## Create plot name
Pieces = TupleFileName.split("/")
FrameFileName = Pieces[(len(Pieces)-1)]
FrameFileName = FrameFileName.replace('.root','_CombinedTrigEff.png')
FrameFilePath = PlotsPath+FrameFileName
print FrameFilePath

## Plot
cv = TCanvas()
cv.SetBottomMargin(0.15)
cv.SetRightMargin(0.15)
h1.Draw('bar')
cv.SaveAs(FrameFilePath)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


