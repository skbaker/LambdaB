# Plots of background fit
Generated using `../TriggerEfficiencies.py` and `CombineTriggers.py`  
Also displayed at http://www.hep.ph.ic.ac.uk/~sb2410/LambdaB/Triggers/  

### Lb -> dp, MagDown
![](./Pure_MC_Lb_dp_md_Reco2015_TrigEff.png)
![](./Pure_MC_Lb_dp_md_Reco2015_CombinedTrigEff.png)
### Lb -> dp, MagUp
![](./Pure_MC_Lb_dp_mu_Reco2015_TrigEff.png)
![](./Pure_MC_Lb_dp_mu_Reco2015_CombinedTrigEff.png)
### Lb -> dppipi, MagDown
![](./Pure_MC_Lb_dppipi_Strip26_md_TrigEff.png)
![](./Pure_MC_Lb_dppipi_Strip26_md_CombinedTrigEff.png)
### Lb -> dppipi, MagUp
![](./Pure_MC_Lb_dppipi_Strip26_mu_TrigEff.png)
![](./Pure_MC_Lb_dppipi_Strip26_mu_CombinedTrigEff.png)
