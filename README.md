# Directory for the work for Lambdab -> dp decay

### ProcessMC
Some files for getting MC with correct stripping and triggers, etc.  
Also script to cut duplicate/misidentified particles from nTuples using MCTruth
information

### StripEfficiency
Find efficiency of stripping and triggers so we can order right amount of MC

### FindBackground
Estimate the background level in the signal region.  
Remove 99% of the Lb signal in MC to find mass window to cut on, then fit 
exponential to data to estimate amount of background in this region.

### Normalisation
Directory for all work for the normalisation channel Lb -> Lc(-> pKpi)pi.

### Data
MC nTuples found at `/eos/lhcb/user/s/skbaker/Lb_dp_Strip26/MC/Tuples/`  
Separate for MagUp and MagDown:
* `MC_Lb_dp_mu_Reco2015.root`
* `MC_Lb_dp_md_Reco2015.root`  

Blinded nTuples in `DataTuples.py`
