from ROOT import *
"""
### Creating training samples
##############################

## Script changed a few times for 2012/2016 data, and for data pre-/post-selection

## Select MC magnet polarity!
## 2012
#f_sig = TFile("/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2012/"\
#              "MagDown_Lb_LcPi_2012.root")
#              "MagUp_Lb_LcPi_2012.root")
##t_sig_all = f_sig.Get("Lb_Lcpi_Tuple/DecayTree")
## 2016
f_sig = TFile("/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/"\
              ## Change polarity for data below too! 
              "Pure_MC_Lb_LcPi_2016_MagDown.root")
              #"Pure_MC_Lb_LcPi_2016_MagUp.root")
t_sig_all = f_sig.Get("DecayTree")

f_bkg = TFile("/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/Tuples/"\
              #"Data_Lb_LcPi_2016.root")
              "Selected_Data_Lb_LcPi_2016.root")
#t_bkg_all = f_bkg.Get("Lb_Lcpi_Tuple/DecayTree")
t_bkg_all = f_bkg.Get("DecayTree")

#fout = TFile("/eos/lhcb/user/s/skbaker/Lb_Lcpi/TMVA/training_samples.root",\
fout = TFile("/eos/lhcb/user/s/skbaker/Lb_Lcpi/TMVA/"\
             "training_samples_2016_postSelection.root",\
             "recreate")
## Cut out backgrounds from (mis)reconstruction in MC 
t_sig = t_sig_all.CopyTree("Lambda_b_BKGCAT < 55")
t_sig.Write("SignalTree")
## Upper mass sideband and magnet polarity 
t_bkg = t_bkg_all.CopyTree("Lambda_b_M > 5900 && Lambda_b_M < 7000 && Polarity==-1")
t_bkg.Write("BkgTree")
"""

### Defining TMVA factory
##############################

## 2012
#fin = TFile('/eos/lhcb/user/s/skbaker/Lb_Lcpi/TMVA/training_samples.root')
## 2016
fin = TFile('/eos/lhcb/user/s/skbaker/Lb_Lcpi/TMVA/'\
            #'training_samples_2016.root')
            'training_samples_2016_postSelection.root')
t_sig = fin.Get("SignalTree") 
t_bkg = fin.Get("BkgTree")
fout = TFile("training.root","recreate")

TMVA.Tools.Instance()

factory = TMVA.Factory('MyClassification', fout,
                            ":".join([
                            "!V",# verbose output
                            "!Silent",# if true, inhibits output
                            "Color",# coloured screen output
                            "DrawProgressBar",# displays schedule
                            "Transformations=I;D;P;G,D",# list of transformations
                            "AnalysisType=Classification"])# set analysis type
                            )

## Take discriminating variables from plots 
#factory.AddVariable("Lambda_b_OWNPV_CHI2","F")
factory.AddVariable("(1-(1-Lambda_b_DIRA_OWNPV)^(1/4))","F")
#factory.AddVariable("Lambda_b_TAU","F")
factory.AddVariable("log(Lambda_b_TAUCHI2)","F")
#factory.AddVariable("Lambda_b_IP_OWNPV","F")
factory.AddVariable("log(Lambda_b_IPCHI2_OWNPV)","F")
factory.AddVariable("log(Lambda_b_FDCHI2_OWNPV)","F")
factory.AddVariable("log(Lambda_b_ENDVERTEX_CHI2)","F")
#factory.AddVariable("Lambda_b_ETA","F")
#factory.AddVariable("Lambda_b_PT","F")
#factory.AddVariable("p_PT","F")
#factory.AddVariable("K_PT","F")
#factory.AddVariable("piplus_PT","F")
#factory.AddVariable("piminus_PT","F")
factory.AddVariable("p_TRACK_CHI2NDOF","F")
factory.AddVariable("log(p_IPCHI2_OWNPV)","F")
factory.AddVariable("piplus_TRACK_CHI2NDOF","F")
factory.AddVariable("log(piplus_IPCHI2_OWNPV)","F")
factory.AddVariable("piminus_TRACK_CHI2NDOF","F")
factory.AddVariable("log(piminus_IPCHI2_OWNPV)","F")
factory.AddVariable("K_TRACK_CHI2NDOF","F")
factory.AddVariable("log(K_IPCHI2_OWNPV)","F")

## Weights can be added to these, for if you want multiple signal and bkg trees
factory.AddSignalTree(t_sig)
factory.AddBackgroundTree(t_bkg)

# cuts
## No cuts
Cut = TCut("1") 
## Cuts to make variable ranges 'physical'
myCut = TCut("Lambda_b_TAU > 0.0 && Lambda_b_TAUCHI2 > 0.0 && "\
             "Lambda_b_OWNPV_CHI2 > 0.0 && Lambda_b_IPCHI2_OWNPV > 0.0 &&"\
             "Lambda_b_DIRA_OWNPV > 0.999 && Lambda_b_TAU < 0.05 &&"\
             "Lambda_b_TAUCHI2 < 30.0 && Lambda_b_IPCHI2_OWNPV < 30.0")

factory.PrepareTrainingAndTestTree(Cut,
                                   #":".join(["nTrain_Signal=2000"
                                   #        , "nTrain_Background=2000"
                                   #        , "nTest_Signal=2000"
                                   #        , "nTest_Background=2000"
                                   ":".join(["nTrain_Signal=0"
                                           , "nTrain_Background=0"
                                           , "SplitMode=Random"
                                           , "NormMode=NumEvents"
                                           , "!V"
                                           ]))
# NormMode=NumEvents: Events in samples scaled to match nTrain_ amounts  

## Defining the classifiers to train
method = factory.BookMethod(TMVA.Types.kBDT, "BDT",
                            ":".join(["!H"
                                    , "!V"
                                    , "NTrees=1000"
                                    , "MinNodeSize=0.5%"
                                    , "MaxDepth=3"
                                    , "BoostType=Grad"#AdaBoost"
                                    , "AdaBoostBeta=0.5"
                                    , "SeparationType=GiniIndex"
                                    , "nCuts=20"
                                    , "PruneMethod=NoPruning"
                                    , "Shrinkage=0.02" # Learning rate
                                    ]))

### Traingin and testing
##############################
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

### Visualising the results
##############################
# TMVA.TMVAGUI("training.root")

