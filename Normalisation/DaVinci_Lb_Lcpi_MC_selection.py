## Running DaVinci over MC to make same selection as data, for the normalisation
## channel, Lb -> Lc (-> pKpi) pi
##
## This is the script taken from running over the MC signal sample
## Adapt it here to run over the contol channel too
## NB: Control channel is 2012 conditions; unlike Lb_dp, it doesn't need rerunning
## with 2015 reconstruction, because deuteron hypothesis not needed
## 
## The stripping does not need to be re-run.
## The input and output will be defined by Ganga.
## Name of ROOT output file needs to match the filename in ganga_DaVinci.py
##
################################
################################
# Things need to be re-written to run on batch
Name       = "MC_Lb_Lcpi"
#datatype   = "2012"
datatype   = "2016"
abs_path   = "/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples"
#jnumber    = "JOBNUMBER"
#filename   = "PATHTOFILE"
#mag        = "MAGNET"
################################
################################
## For running locally: (with one input file)
jnumber    = "001"
#filename   = "/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/DST/"\
#             "SingleDST_MagDown_00044496_00000001_2.dst"
mag        = "md"
################################
################################

from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import LoKi__HDRFilter

#------------------------------------------------------------------------------
# Fill tuple
from Configurables import ( MCDecayTreeTuple
                          , DecayTreeTuple
                          , EventTuple
                          , TupleToolTrigger
                          , TupleToolTISTOS
                          , FilterDesktop
                          , TupleToolStripping
                          , TupleToolMCTruth
                          , TupleToolKinematic
                          , TupleToolMCBackgroundInfo
                          , MCTupleToolDecayType
                          , MCTupleToolEventType
                          #, MCTupleToolBremInfo
                          )
from Configurables import ( BackgroundCategory
                          , TupleToolDecay
                          , TupleToolVtxIsoln
                          , TupleToolPid
                          , EventCountHisto
                          , TupleToolRecoStats
                          )

from Configurables import TupleToolDecayTreeFitter
from Configurables import LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *

tuple = DecayTreeTuple("Lb_Lcpi_Tuple")

## 2012
#tuple.Inputs = ["/Event/AllStreams/Phys/Lb2LcPiLc2PKPiBeauty2CharmLine/Particles"]
## 2016
tuple.Inputs = ["Phys/Lb2LcPiLc2PKPiBeauty2CharmLine/Particles"]

tuple.Decay = "[Lambda_b0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^pi-]CC"

tuple.addBranches({
                   "Lambda_b":"[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi-]CC"
                 , "Lambda_c":"[Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) pi-]CC"
                 , "p"       :"[Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) pi-]CC"
                 , "K"       :"[Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) pi-]CC"
                 , "piplus"  :"[Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) pi-]CC"
                 , "piminus" :"[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^pi-]CC"
                 })
                 ## Want to change this selection, so that the piplus is always
                 ## the Lc daughter, and piminus is always the Lb daughter, as
                 ## above, for 2016 MC. Was as below for 2012.
                 #, "piplus"  :"[(Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) pi-),"\
                 #              "(Lambda_b~0 -> (Lambda_c~- -> p~- K+ pi-) ^pi+)]"
                 #, "piminus" :"[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^pi-),"\
                 #              "(Lambda_b~0 -> (Lambda_c~- -> p~- K+ ^pi-) pi+)]"
                 #})


for particle in ["Lambda_c","p","K","piplus","piminus"]:
    tuple.addTool(TupleToolDecay, name = particle)

tuple.ToolList += [ "TupleToolAngles"
                  , "TupleToolEventInfo"
                  , "TupleToolGeometry"
                  , "TupleToolKinematic"
                  , "TupleToolPid"
                  , "TupleToolPrimaries"
                  , "TupleToolPropertime"
                  , "TupleToolTrackInfo"
                  , "TupleToolMCTruth"
                  , "TupleToolStripping"
                  ]

tuple.addTool(TupleToolPid, name="TupleToolPid")
tuple.TupleToolPid.Verbose = True

## Reference point
tuple.ToolList+=[ "TupleToolKinematic" ]
tuple.addTool(TupleToolKinematic, name="TupleToolKinematic")
tuple.TupleToolKinematic.Verbose = True

## Stipping tool
## Added at last minute just to find stripping efficiency
## Not used in full running of DaVinci for all MC
DecisionList = ["Phys/Lb2LcPiLc2PKPiBeauty2CharmLine"]

tuple.addTool(TupleToolStripping, name="TupleToolStripping")
tuple.TupleToolStripping.StrippingList = DecisionList

## Background category
tuple.addTool(TupleToolMCBackgroundInfo, name = "TupleToolMCBackgroundInfo")
tuple.TupleToolMCBackgroundInfo.addTool(BackgroundCategory())
tuple.ToolList += ["TupleToolMCBackgroundInfo"]

## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [ "MCTupleToolAngles"
                    ,"MCTupleToolHierarchy"
                    ,"MCTupleToolKinematic" ]
tuple.addTool(MCTruth, name="TupleToolMCTruth")

## Triggers
_trigger_list = [
    #L0
    'L0HadronDecision'
    ,'L0ElectronDecision'
    ,'L0ElectronHiDecision'
    ,'L0MuonDecision'
    ,'L0DiMuonDecision'
    ,'L0MuonHighDecision'
    ,'L0PhotonDecision'
    #Hlt1
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonNoIPDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1MultiMuonNoL0Decision'
    ,'Hlt1CalibMuonAlignJpsiDecision'
    ,'Hlt1DiMuonNoL0Decision'
    ,'Hlt1TrackMVADecision'
    ,'Hlt1TwoTrackMVADecision'
    ,'Hlt1B2GammaGammaDecision'
    ,'Hlt1B2HH_LTUNB_KKDecision'
    ,'Hlt1B2HH_LTUNB_KPiDecision'
    ,'Hlt1B2HH_LTUNB_PiPiDecision'
    ,'Hlt1B2PhiGamma_LTUNBDecision'
    ,'Hlt1B2PhiPhi_LTUNBDecision'
    ,'Hlt1BeamGasBeam1Decision'
    ,'Hlt1BeamGasBeam2Decision'
    ,'Hlt1BeamGasCrossingEnhancedBeam1Decision'
    ,'Hlt1BeamGasCrossingEnhancedBeam2Decision'
    ,'Hlt1BeamGasCrossingForcedRecoDecision'
    ,'Hlt1BeamGasCrossingForcedRecoFullZDecision'
    ,'Hlt1BeamGasHighRhoVerticesDecision'
    ,'Hlt1BeamGasNoBeamBeam1Decision'
    ,'Hlt1BeamGasNoBeamBeam2Decision'
    ,'Hlt1CEPDecision'
    ,'Hlt1CEPVeloCutDecision'
    ,'Hlt1CalibHighPTLowMultTrksDecision'
    ,'Hlt1CalibRICHMirrorRICH1Decision'
    ,'Hlt1CalibRICHMirrorRICH2Decision'
    ,'Hlt1CalibTrackingKKDecision'
    ,'Hlt1CalibTrackingKPiDecision'
    ,'Hlt1CalibTrackingKPiDetachedDecision'
    ,'Hlt1CalibTrackingPiPiDecision'
    #,'Hlt1DiProtonDecision'
    #,'Hlt1DiProtonLowMultDecision'
    ,'Hlt1IncPhiDecision'
    ,'Hlt1L0AnyDecision'
    ,'Hlt1L0AnyNoSPDDecision'
    ,'Hlt1LumiDecision'
    ,'Hlt1MBNoBiasDecision'
    ,'Hlt1MBNoBiasRateLimitedDecision'
    ,'Hlt1NoBiasNonBeamBeamDecision'
    ,'Hlt1NoPVPassThroughDecision'
    ,'Hlt1ODINTechnicalDecision'
    ,'Hlt1SingleElectronNoIPDecision'
    ,'Hlt1Tell1ErrorDecision'
    ,'Hlt1TrackMuonNoSPDDecision'
    ,'Hlt1VeloClosingMicroBiasDecision'
    ,'Hlt1BeamGasCrossingParasiticDecision'
    ,'Hlt1ErrorEventDecision'
    ,'Hlt1GlobalDecision'
    #Hlt2
    ,'Hlt2DebugEventDecision'
    ,'Hlt2DiMuonBDecision'
    ,'Hlt2DiMuonBTurboDecision'
    ,'Hlt2DiMuonBTurboDecision'
    ,'Hlt2DiMuonDetachedDecision'
    ,'Hlt2DiMuonDetachedHeavyDecision'
    ,'Hlt2DiMuonDetachedJPsiDecision'
    ,'Hlt2DiMuonDetachedPsi2SDecision'
    ,'Hlt2DiMuonJPsiDecision'
    ,'Hlt2DiMuonJPsiHighPTDecision'
    ,'Hlt2DiMuonJPsiTurboDecision'
    ,'Hlt2DiMuonJPsiTurboDecision'
    ,'Hlt2DiMuonPsi2SDecision'
    ,'Hlt2DiMuonPsi2SHighPTDecision'
    ,'Hlt2DiMuonPsi2STurboDecision'
    ,'Hlt2DiMuonPsi2STurboDecision'
    ,'Hlt2DiMuonSoftDecision'
    ,'Hlt2DiMuonZDecision'
    ,'Hlt2EWDiElectronDYDecision'
    ,'Hlt2EWDiElectronHighMassDecision'
    ,'Hlt2EWDiMuonDY1Decision'
    ,'Hlt2EWDiMuonDY2Decision'
    ,'Hlt2EWDiMuonDY3Decision'
    ,'Hlt2EWDiMuonDY4Decision'
    ,'Hlt2EWDiMuonZDecision'
    ,'Hlt2EWSingleElectronHighPtDecision'
    ,'Hlt2EWSingleElectronLowPtDecision'
    ,'Hlt2EWSingleElectronVHighPtDecision'
    ,'Hlt2EWSingleMuonHighPtDecision'
    ,'Hlt2EWSingleMuonLowPtDecision'
    ,'Hlt2EWSingleMuonVHighPtDecision'
    ,'Hlt2EWSingleTauHighPt2ProngDecision'
    ,'Hlt2EWSingleTauHighPt3ProngDecision'
    ,'Hlt2ForwardDecision'
    ,'Hlt2SingleMuonDecision'
    ,'Hlt2SingleMuonHighPTDecision'
    ,'Hlt2SingleMuonLowPTDecision'
    ,'Hlt2SingleMuonNoSPDDecision'
    ,'Hlt2SingleMuonRareDecision'
    ,'Hlt2SingleMuonVHighPTDecision'
    ,'Hlt2Topo2BodyDecision'
    ,'Hlt2Topo3BodyDecision'
    ,'Hlt2Topo4BodyDecision'
    ,'Hlt2TopoMu2BodyDecision'
    ,'Hlt2TopoMu3BodyDecision'
    ,'Hlt2TopoMu4BodyDecision'
    ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalibDecision'
    ,'Hlt2TransparentDecision'
    ,'Hlt2ErrorEventDecision'
    ,'Hlt2GlobalDecision'
    ]

tuple.ToolList+=[ "TupleToolTISTOS" ]
tuple.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
tuple.TupleToolTISTOS.Verbose = True
tuple.TupleToolTISTOS.VerboseL0 = True
tuple.TupleToolTISTOS.VerboseHlt1 = True
tuple.TupleToolTISTOS.VerboseHlt2 = True
tuple.TupleToolTISTOS.TriggerList = _trigger_list #trigger_list.trigger_list()

from Configurables import TupleToolRecoStats
tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
tuple.TupleToolRecoStats.Verbose=True

LoKi_All = tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = { 'ETA' : 'ETA' }

#------------------------------------------------------------------------------
# Input
## The input is configured automatically when submitted through Ganga, so these
## lines are not needed
#from GaudiConf import IOHelper
#IOHelper().inputFiles(['00056651_00000001_3.AllStreams.mdst'],clear=True)

#------------------------------------------------------------------------------
# DaVinci Configuration

from Configurables import DaVinci
#DaVinci().RootInTES = "/Event/Bhadron"
DaVinci().RootInTES = "/Event/AllStreams"
#DaVinci().InputType = "DST"
DaVinci().InputType = "MDST"
DaVinci().DataType = datatype
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().EvtMax = -1
#DaVinci().EvtMax = 500 
## Ignore histogram files for Ganga submission
#DaVinci().HistogramFile = abs_path+"/DVHistos_"+mag+"_"+jnumber+".root"
DaVinci().TupleFile = "MC_Lb_LcPi_2016.root" 
#abs_path+"/"+Name+"_"+mag+"_"+jnumber+".root"
DaVinci().UserAlgorithms  = [ tuple ]

# database
#DaVinci().DDDBtag   = "dddb-20130929-1"
#DaVinci().CondDBtag = "sim-20130522-1-vc-"+mag+"100"
DaVinci().DDDBtag = "dddb-20150724"
DaVinci().CondDBtag = "sim-20161124-2-vc-md100"

