#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Script to apply same selection criteria as in the previous measurement
## of this channel, to see if the results are matched.
##
## Follow LHCb-ANA-2013-023
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from __future__ import division
import re
import string
import sys
#print sys.argv[1] # argument with python command

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

purple = TColor.GetColor('#5e3c99')
orange = TColor.GetColor('#e66101')

## Path to write plots too
PlotsPath = '/afs/cern.ch/user/s/skbaker/LambdaB/Normalisation/Plots/'

## Run ROOT in batch mode
#gROOT.SetBatch(True)

## List of branches to use
names = [ 'Lambda_b_M'
        , 'Lambda_b_IPCHI2_OWNPV'
        , 'Lambda_b_L0HadronDecision_TOS'
        , 'Lambda_b_L0Global_TIS'
        , 'Lambda_b_Hlt1Phys_TOS'
        , 'Lambda_b_Hlt2Phys_TOS'
        , 'Lambda_b_Hlt1TrackMVADecision_TOS'
        , 'Lambda_b_Hlt2Topo2BodyDecision_TOS'
        , 'Lambda_b_Hlt2Topo3BodyDecision_TOS'
        , 'Lambda_b_Hlt2Topo4BodyDecision_TOS'
        , 'Lambda_c_M'
        , 'piminus_PIDK'
        , 'piplus_PIDK'
        , 'K_PIDK'
        , 'p_PIDp'
        , 'p_PIDK'
        ## Additional variables for BDT:
        , 'Lambda_b_OWNPV_CHI2'
        , 'Lambda_b_DIRA_OWNPV'
        , 'Lambda_b_TAU'
        , 'Lambda_b_TAUCHI2'
        , 'Lambda_b_FDCHI2_OWNPV'
        , 'Lambda_b_ENDVERTEX_CHI2'
        , 'p_TRACK_CHI2NDOF'
        , 'p_IPCHI2_OWNPV'
        , 'piplus_TRACK_CHI2NDOF'
        , 'piplus_IPCHI2_OWNPV'
        , 'piminus_TRACK_CHI2NDOF'
        , 'piminus_IPCHI2_OWNPV'
        , 'K_TRACK_CHI2NDOF'
        , 'K_IPCHI2_OWNPV'
        , 'Polarity'
        ## PID variables for LHCb-ANA-2016-030 selection
        , 'p_ProbNNp'
        , 'K_ProbNNpi'
        , 'piplus_ProbNNpi'
        , 'piminus_ProbNNpi'
        , 'K_ProbNNk'
        , 'piplus_ProbNNk'
        , 'piminus_ProbNNk'
        ## isMuon
        , 'K_isMuon'
        , 'piplus_isMuon'
        , 'piminus_isMuon'
        , 'p_isMuon'
        ## pT
        , 'K_PT'
        , 'p_PT'
        , 'piplus_PT'
        , 'piminus_PT'
        ]


## Read in nTuples
DataFileName = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/Tuples/'\
               'Data_Lb_LcPi_2016.root' 
## 2012 
#MCFileName = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2012/'\
#             'Pure_MagDown_Lb_LcPi_2012.root'
## 2016
MCFileName = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/'\
             'Pure_MC_Lb_LcPi_2016_MagDown.root'
             #'Pure_MC_Lb_LcPi_2016_MagUp.root'

MCfile = TFile(MCFileName)
MC = MCfile.Get("DecayTree")
MC.SetBranchStatus("*",0)
for name in names:
    MC.SetBranchStatus(name,1)

Datafile = TFile(DataFileName)
Data = Datafile.Get("Lb_Lcpi_Tuple/DecayTree")
Data.SetBranchStatus("*",0)
for name in names:
    Data.SetBranchStatus(name,1)

## Print entries in trees
print Data.GetEntries()
print MC.GetEntries()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Selection criteria - inspired by LHCb-ANA-2016-030 and LHCb-ANA-2013-023

L0Trigger ='(Lambda_b_L0HadronDecision_TOS == 1 || Lambda_b_L0Global_TIS == 1)'

HLT = '(Lambda_b_Hlt1Phys_TOS == 1 && Lambda_b_Hlt2Phys_TOS == 1)'

#Stripping = Already done
#BDTG > 0.66 # Need to run the BDT first, and find optimal cut value

CharmMass = '(Lambda_c_M > 2265 && Lambda_c_M < 2305)'
BeautyMass = '(Lambda_b_M > 5350 && Lambda_b_M < 6000)'

## Cuts from LHCb-ANA-2016-030
## Cut on isMuon for pairs of oppositely charged tracks
isMuonA = '!(K_isMuon && p_isMuon)'
isMuonB = '!(piplus_isMuon && piminus_isMuon)'
isMuonC = '!(K_isMuon && piplus_isMuon)'
isMuonD = '!(p_isMuon && piminus_isMuon)'
## Cut on isMuon of track with highest pT
isMuonpT = '(((p_PT > K_PT) && (p_PT > piplus_PT) && (p_PT > piminus_PT))'\
           ' && !(p_isMuon)) || '\
           '(((K_PT > p_PT) && (K_PT > piplus_PT) && (K_PT > piminus_PT))'\
           ' && !(K_isMuon)) || '\
           '(((piplus_PT > K_PT) && (piplus_PT > p_PT) && '\
           '(piplus_PT > piminus_PT)) && !(piplus_isMuon)) || '\
           '(((piminus_PT > K_PT) && (piminus_PT > p_PT) && '\
           '(piminus_PT > piplus_PT)) && !(piminus_isMuon))'
## Total isMuon cut
isMuon = '('+isMuonA+' && '+isMuonB+' && '+isMuonC+' && '+isMuonD+' && '\
         +isMuonpT+')'

## 2013 PID
BachelorPiPID = '(piminus_PIDK < 0)'
CharmPiPID = '(piplus_PIDK < 5)'
KaonPID = '(K_PIDK > 0)'
ProtonPID = '((p_PIDp > 5 && (p_PIDp - p_PIDK) > 0))'

## PID selection from LHCb-ANA-2016-030
## Higher cut here favours pion selection, but not kaon selection. Seeing as
## there are two pions, better to favour pions than kaons
## Should run full optimisation for this, however
BaseCut = 0.15 # PID efficiency = 0.48
NewCut = 0.25  # eff = 0.55
MoreCut = 0.35 # eff = 0.607
Cut = MoreCut

KaonPID2016 = '((K_ProbNNpi - 1)*(K_ProbNNpi - 1) + (K_ProbNNk)*(K_ProbNNk)) >'\
              +str(Cut)
PionPPID2016 = '((piplus_ProbNNpi - 1)*(piplus_ProbNNk - 1) + '\
               '(piplus_ProbNNk)*(piplus_ProbNNk)) < '+str(Cut)
PionMPID2016 = '((piminus_ProbNNpi - 1)*(piminus_ProbNNk - 1) + '\
               '(piminus_ProbNNk)*(piminus_ProbNNk)) < '+str(Cut)
ProtonPID2016 = 'p_ProbNNp > 0.5'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Make cuts on the trees, following the AnaNote, and see what remains
"""
selection2013 = KaonPID + '&&' + ProtonPID  + '&&' + CharmPiPID\
            + '&&' + BachelorPiPID + '&&' + CharmMass + '&&' + BeautyMass\
            + '&&' + L0Trigger + '&&' + HLT 
print selection2013
selection2016 = KaonPID2016 + '&&' + ProtonPID2016  + '&&' + PionPPID2016\
            + '&&' + PionMPID2016 + '&&' + CharmMass + '&&' + BeautyMass\
            + '&&' + L0Trigger + '&&' + HLT 
print selection2016

## Choose 2013 or 2016 ANA note selections
selection = selection2016
print selection

cv = TCanvas()
cv.SetTicks()
cv.SetRightMargin(0.05)
cv.SetLeftMargin(0.11)
h1 = TH1F('h1','h1',300,5000,6500)
h1.SetFillColor(orange)
h1.SetLineColor(orange)
h1.SetFillStyle(3020)
h1.GetXaxis().SetTitle('M_{#Lambda_{b}} [MeV/c^{2}]')
h1.GetYaxis().SetTitle('Candidates /(5 MeV/c^{2})')
h1.GetXaxis().SetTitleOffset(1.25)
h1.GetYaxis().SetTitleOffset(1.45)
Data.Draw('Lambda_b_M>>h1')

h2 = TH1F('h2','h2',300,5000,6500)
h2.SetFillColor(purple)
h2.SetLineColor(purple)
h2.SetFillStyle(3008)
Data.Draw('Lambda_b_M>>h2',selection)

h1.Draw()
h2.Draw('same')

#cv.SaveAs(PlotsPath+'Selection_Lb_Lcpi_Data.png')
cv.SaveAs(PlotsPath+'Selection_ANA2016_Lb_Lcpi_Data.png')

## Make new nTuple with this selected data
NewFileName = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/Tuples/'\
              'Selected_ANA2016_Data_Lb_LcPi_2016.root'
              #'Selected_Data_Lb_LcPi_2016.root'
SelectFile = TFile(NewFileName,'recreate')
## Only the active branches are copied here, so not the full tree!
newTree = Data.CopyTree(selection)
print Data.GetEntriesFast()
print newTree.GetEntriesFast
SelectFile.Write()

raw_input("Press ENTER to continue")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Make same selection on truth-matched signal MC, to find selection efficiency 

c1 = TCanvas()
c1.SetTicks()
c1.SetRightMargin(0.05)
c1.SetLeftMargin(0.11)
h3 = TH1F('h3','h3',300,5000,6500)
h3.SetFillColor(orange)
h3.SetLineColor(orange)
h3.SetFillStyle(3020)
h3.GetXaxis().SetTitle('M_{#Lambda_{b}} [MeV/c^{2}]')
h3.GetYaxis().SetTitle('Candidates /(5 MeV/c^{2})')
h3.GetXaxis().SetTitleOffset(1.25)
h3.GetYaxis().SetTitleOffset(1.45)
MC.Draw('Lambda_b_M>>h3')

h4 = TH1F('h4','h4',300,5000,6500)
h4.SetFillColor(purple)
h4.SetLineColor(purple)
h4.SetFillStyle(3008)
MC.Draw('Lambda_b_M>>h4',selection)

h3.Draw()
h4.Draw("same")

leg2 = TLegend(0.15,0.78,0.45,0.85)
leg2.SetFillStyle(0)
leg2.SetLineWidth(0)
leg2.SetHeader('Cut eff: '+('{:01.3f}'.\
                       format((MC.GetEntries(selection)/MC.GetEntries()))))
leg2.Draw()

#c1.SaveAs(PlotsPath+'Selection_Lb_Lcpi_MC2012.png')
#c1.SaveAs(PlotsPath+'Selection_Lb_Lcpi_MC2016.png')
c1.SaveAs(PlotsPath+'Selection_ANA2016_Lb_Lcpi_MC2016.png')

## Make new nTuple with this selected MC 
NewMCFileName = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/'\
              'Selected_ANA2016_MC_Lb_LcPi_2016.root'
              #'Selected_MC_Lb_LcPi_2016.root'
SelectMCFile = TFile(NewMCFileName,'recreate')

## Write selected pure MC to file too, so fit to this, instead of full pure MC
newMCTree = MC.CopyTree(selection)
print MC.GetEntriesFast()
print newMCTree.GetEntriesFast
SelectMCFile.Write()

raw_input("Press ENTER to continue")
"""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Want to print the efficiency of each cut, sequencially
Cuts = [L0Trigger, HLT, BeautyMass, CharmMass, isMuon,
        PionMPID2016, PionPPID2016, KaonPID2016, ProtonPID2016]

print 'No cuts: '+str(MC.GetEntriesFast())
WholeTree = MC.GetEntriesFast()
passed = WholeTree 
previous = passed

## Vector to fill with cuts sequentially
EmptyCuts = []

## Compute number passing sequential cuts
## What order should the cuts be in? Seek to match LHCb-ANA-2016-030
for cut in Cuts:
    EmptyCuts.append(cut)
    Length = len(EmptyCuts)
    Sequence = '1'
    for i in range(0,Length):
        Sequence += ' && '+ EmptyCuts[i]
    new = MC.GetEntries(Sequence)
    #print new
    #print previous
    efficiency = float(new/previous)
    ## Printing formatted for README.md table
    print '|`'+cut+'`|'+str(efficiency)+'|'
    previous = new 

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
