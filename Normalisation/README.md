# Normalisation
Normalisation channel used in order to find the Lb -> dp branching fraction.  

### Lb -> Lc(pKpi)pi, 15264011.  
Channel used as normalisation in other branching fraction measurements,
see ANA NOTE: LHCB-ANA-2016-030  
https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Xb2phhh\_BF  

Branching fraction ~ 0.5% x 6%, measured by LHCb https://arxiv.org/pdf/1405.6842.pdf  

Existing data and MC are for 2012 conditions.

### 2013 analysis
https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/B2OC\_fLbfd

### DaVinci\_Lb\_Lcpi\_Data.py
DaVinci script for 2016 data.

### DaVinci\_Lb\_Lcpi\_MC\_selection.py
Don't need to re-run stripping here, just select the decay.  
This needs to be run on Ganga, so that it will access the LFN of the DST files.
This is better than downloading files and running on batch.

### ganga\_MC\_DaVinci.py
Script to submit the DaVinci options file to the grid.  
The data that is chosen needs changing in here, to run over all DSTs.  
The nTuples will need merging afterwards. They can be found in ~/gangadir/ on 
lxplus.  

### ganga\_Data\_DaVinci.py
Script for data submission to make nTuples on grid.  
This script also selects the data based on run number, so only [180861 - 185611].

### Paula\_ganga.py
Script from Paula to get me started with submitting DaVinci to Ganga.

### nTuples
NTuples can be found at:
* 2012: `/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2012/MagUp_Lb_LcPi_2012.root`
* 2012: `/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2012/MagDown_Lb_LcPi_2012.root`
* 2016: `/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/MC_Lb_LcPi_2016_MagDown.root`
* 2016: `/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/MC_Lb_LcPi_2016_MagUp.root`
* 2016 Pure: `/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/`\
`Pure_MC_Lb_LcPi_2016_MagDown.root`
* 2016 Pure: `/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/`\
`Pure_MC_Lb_LcPi_2016_MagUp.root`

### NormMCTuples.py
List of nTuples to be read by bash scripts to input MC.

### RunScripts.sh
Run this command to run scripts over both sets of MC ntuples for MagUp and MagDown.

```
## Before running python scripts below on lxplus, do:
SetupProject Urania v5r0
```

### TriggerEfficiencies.py
Plot the efficiencies of a few triggers.  
Currently is same triggers as for signal MC.

### Discriminants.py
Script to plot discriminating varibles in signal MC and data bkg.

### TrainTMVA.py
Script to start training the BDT. Then use it to run over samples.

### TrainSKLearn.py
Script to use scikit-learn for machine learning instead. Could be better than
TMVA, as XGBoost is available. Requires some setting up of the environment
before running this script. 
```
source /cvmfs/sft.cern.ch/lcg/views/LCG_91/x86_64-slc6-gcc62-opt/setup.sh
SetupProject Urania v5r0
```
Outputs plots shown below.   
Needs a bit more work on this script. See also iPython Notebooks in SWAN.

### CutOnMCTruth.py
Script to remove incorrectly reconstructed particles from MC and make _pure_ 
samples.  

### Selection.py
Script to apply the selection from LHCb-ANA-2013-023 to the Lb -> LcPi data
and MC, to remove most of the background.   
The fit used after this selection proved too tricky, as the needed MC was not
available for backgrounds.
Therefore, use selection from LHCb-ANA-2016-030.

Lines added to calculate efficiencies of each set of cuts. Compare these to
LHCb-ANA-2016-030.


Step-by-step efficiencies calculated w.r.t. previous cut:

|Cut | Efficiency |LHCb-ANA-2016-030 Eff.|
|--- | --- | --- |
|Repo. \+ stripping | ~ | 0.03|
|`(Lambda_b_L0HadronDecision_TOS == 1 OR Lambda_b_L0Global_TIS == 1)`|0.970986460348|0.333|
|`(Lambda_b_Hlt1Phys_TOS == 1 && Lambda_b_Hlt2Phys_TOS == 1)`|0.996164574616|0.85|
|`(Lambda_b_M > 5350 && Lambda_b_M < 6000)`|0.995012250613|~|
|`(Lambda_c_M > 2265 && Lambda_c_M < 2305)`|0.972737665992|0.82|
|`((piminus_ProbNNpi - 1)*(piminus_ProbNNk - 1) + (piminus_ProbNNk)*(piminus_ProbNNk)) < 0.35`|0.74886746988| ~ |
|`((piplus_ProbNNpi - 1)*(piplus_ProbNNk - 1) + (piplus_ProbNNk)*(piplus_ProbNNk)) < 0.35`|0.93654675333| ~ |
|`((K_ProbNNpi - 1)*(K_ProbNNpi - 1) + (K_ProbNNk)*(K_ProbNNk)) >0.35`|0.940081082938| ~ |
|`p_ProbNNp > 0.5`|0.922447189533| ~ |
|Total PID| 0.607| 0.48494|
|BDT | ~| 0.6658|
|`!(K_isMuon && p_isMuon) && !(piplus_isMuon && piminus_isMuon) && !(K_isMuon && piplus_isMuon) && !(p_isMuon && piminus_isMuon) && (((p_PT > K_PT) && (p_PT > piplus_PT) && (p_PT > piminus_PT)) && !(p_isMuon)) OR (((K_PT > p_PT) && (K_PT > piplus_PT) && (K_PT > piminus_PT)) && !(K_isMuon)) OR (((piplus_PT > K_PT) && (piplus_PT > p_PT) && (piplus_PT > piminus_PT)) && !(piplus_isMuon)) OR (((piminus_PT > K_PT) && (piminus_PT > p_PT) && (piminus_PT > piplus_PT)) && !(piminus_isMuon))`|0.997452290535| 0.99|
|Total| 0.5683 (without repo. \+ stripping)| 0.004 (without PID)|

Need to find the Repo and stripping efficiencies, plus generator level 
efficiencies?  
MC stats: 
https://twiki.cern.ch/twiki/bin/view/LHCb/DownloadAndBuild#2\_Produce\_the\_tables  
https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/GeneratorFAQ   
2016 MC is under request ID 35404.

### Fits.py
Script to use RooFit to fit to Lb -> Lc pi signal.
Fit double Crystal Ball function to MC signal to find shape parameters, then
fit this, an exponential (combinatorial) and an Argus(X)Gauss (part. reco.) to
the selected data.

#### Comparing fit results to 2013 result:
2013, @ 7TeV, 1 fb^-1 = 44859 candidates in fit (LHCb-ANA-2013-023) 
2016, @ 13 TeV, 0.845 fb-1 = 81731 candidates in fit  
From 2013 result, would expect twice as many Lb at 13 TeV compared to 7 TeV,
so get (2 x 44859) x 0.845 = 75811.7.  
Therefore, 2016 result from our fit looks reasonable.

### GetLumi.py
Quick script to compute and output the integrated luminosity of the data file.
`Total integrated luminosity: 845.09 +/- 845.09 pb^-1`  
Get this due to `DaVinci().Lumi = True` in processing script.
Is this what should be used to compare yield to 2013 result?  

Taken from here: 
https://lhcbqa.web.cern.ch/lhcbqa/418/how-to-get-the-luminosity-of-a-data-tuple


![](ROC_Curve.png)
![](ROC_Curve_sklearn.png)
![](Sklearn_BDT.png)



