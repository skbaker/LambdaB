#!/bin/bash

cd /afs/cern.ch/user/s/skbaker/LambdaB/Normalisation/

# Need to setup LHCb environment for PyROOT to run
SetupProject Urania v5r0

iter=1
# Loop through list of blind data tuples for Lb -> dp, dppipi
while IFS='' read -r line || [[ -n "$line" ]]; do
    python TriggerEfficiencies.py $line

    ((iter++))
done < NormMCTuples.py

