# Copy MC nTuples, such that only the true particles are selected
# i.e. that protons are protons, and their mother is the Lc
#
# If PyROOT will not run, go into an LHCb environment to set it up
# e.g. $ SetupProject Urania v5r0
#
# Run with magnet orientation argument: MagUp, MagDown
# i.e. $ python CutOnMCTruth.py MagDown 
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Command line argument input
import sys
#print sys.argv[0] # prints CutOnMCTruth.py
#print sys.argv[1] # prints argument

from ROOT import *

# Run ROOT in batch mode
gROOT.SetBatch(True)

# Tuple covering full mass range
## 2012 samples
#FullTupleFile = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2012/'\
#                '{}_Lb_LcPi_2012.root'.format(sys.argv[1])
## 2016 samples
FullTupleFile = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/'\
                'MC_Lb_LcPi_2016_{}.root'.format(sys.argv[1])

print FullTupleFile

original = TFile(FullTupleFile)
oldTree = original.Get('Lb_Lcpi_Tuple/DecayTree')

# New file for cut tree
## 2012
#ClonedTupleFile = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2012/'\
#                  'Pure_{}_Lb_LcPi_2012.root'.format(sys.argv[1])
## 2016
ClonedTupleFile = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/'\
                  'Pure_MC_Lb_LcPi_2016_{}.root'.format(sys.argv[1])

cloned = TFile(ClonedTupleFile,'recreate')
newTree = oldTree.CopyTree('abs(p_TRUEID) == 2212'\
                       ' && abs(p_MC_MOTHER_ID) == 4122'\
                       ' && abs(p_MC_GD_MOTHER_ID) == 5122'\
                       ' && abs(K_TRUEID) == 321'\
                       ' && abs(K_MC_MOTHER_ID) == 4122'\
                       ' && abs(K_MC_GD_MOTHER_ID) == 5122'\
                       ' && abs(piplus_TRUEID) == 211'\
                       ' && abs(piplus_MC_MOTHER_ID) == 4122'\
                       ' && abs(piplus_MC_GD_MOTHER_ID) == 5122'\
                       ' && abs(piminus_TRUEID) == 211'\
                       ' && abs(piminus_MC_MOTHER_ID) == 5122'\
                       )

print 'Before cut'
print oldTree.GetEntriesFast()
print 'After cut'
print newTree.GetEntriesFast()

cloned.Write()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
