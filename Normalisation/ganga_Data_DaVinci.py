#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Run DaVinci on the grid
## Script used to process DST files for 2016 Data into nTuples.
##
## ROOT nTuples must be merged afterwards
##
## Name of ROOT nTuples needs to match name in DaVinci script
##
## Run as: $ ganga ganga_DaVinci.py
## 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Sumbmission functions copied from Matt:
def sendJob(filename, dataset, name):
    j = Job()
    ## Use same DaVinci version as in bkk for the data
    j.application = DaVinci(version='v41r4p1')
    j.application.optsfile = [ filename ]
    j.backend = Dirac()
    j.inputdata = dataset
    j.splitter = SplitByFiles(filesPerJob=20)
    #j.outputfiles = [ LocalFile('Data_Lb_LcPi_2016.root') ]
    j.outputfiles = [ DiracFile('Data_Lb_LcPi_2016.root') ]
    j.parallel_submit = True
    j.name = name
    j.submit()

def getFromBookkeeping(datapath):
    bkk = BKQuery(datapath)
    data = bkk.getDataset()
    return data

def getFromBookkeeping_selectRuns(runpath):
    bkk = BKQuery( dqflag = "OK",
                   path = runpath,
                   type = "Run"
                 )
    data = bkk.getDataset()
    return data

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Try submitting job using Matt's method
#firstDataSetPath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28/90000000/BHADRON.MDST'
firstDataSetPath = '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28/90000000/BHADRON.MDST'
## To only select runs for which the deuteron hypothesis was initialised, use
## the following bkk path to the data.
## Will need to separate by polarity at a later stage
runSelectionDataPath = '180861-185611/Real Data/Reco16/Stripping28/90000000/BHADRON.MDST'

#Data = getFromBookkeeping(firstDataSetPath)
Data = getFromBookkeeping_selectRuns(runSelectionDataPath)
print Data

## Modify so that Data is only one file, for trial
## REMOVE this line to submit the full dataset
#Data.files = Data.files[0:1]
#print Data

sendJob('DaVinci_Lb_Lcpi_Data.py', Data, firstDataSetPath)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
