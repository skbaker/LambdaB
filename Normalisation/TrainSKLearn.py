# Script first tested in SWAN notebook.
# Follow guidance and example by Andrew
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import sklearn
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, auc, roc_auc_score

from xgboost.sklearn import XGBClassifier

import numpy as np
import root_numpy
from root_numpy import root2array, rec2array

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

# Discriminating variables
variables = ["Lambda_b_DIRA_OWNPV"
            ,"Lambda_b_TAU"
            ,"Lambda_b_IPCHI2_OWNPV"
            ,"Lambda_b_FDCHI2_OWNPV"
            ,"Lambda_b_ENDVERTEX_CHI2"
            ,"Lambda_b_ETA"
            ,"Lambda_b_PT"
            ,"p_PT"
            ,"p_IPCHI2_OWNPV"
            ,"p_TRACK_CHI2NDOF"
            ,"K_PT"
            ,"K_IPCHI2_OWNPV"
            ,"K_TRACK_CHI2NDOF"
            ,"piplus_PT"
            ,"piplus_IPCHI2_OWNPV"
            ,"piplus_TRACK_CHI2NDOF"
            ,"piminus_PT"
            ,"piminus_IPCHI2_OWNPV"
            ,"piminus_TRACK_CHI2NDOF"
            ]

TreeFile = '/eos/lhcb/user/s/skbaker/Lb_Lcpi/TMVA/training_samples.root'
signal = root2array(TreeFile,'SignalTree',variables)
signal = rec2array(signal)
background = root2array(TreeFile,'BkgTree',variables)
background = rec2array(background)

# Force signal and background samples to be same size
background = background[:len(signal)]

# x = matrix of feature values, y = array of target values
x = np.concatenate((signal,background))
y = np.concatenate((np.ones(signal.shape[0]),np.zeros(background.shape[0])))

# Split into training and test sets
x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.2,random_state=42)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Define classifiers
bdt = GradientBoostingClassifier(n_estimators=50, learning_rate=0.25, max_leaf_nodes=20, max_features=None, verbose=1)
abc = AdaBoostClassifier(n_estimators=1000, learning_rate=0.3)
rf = RandomForestClassifier(n_estimators=5000,max_leaf_nodes=100,n_jobs=-1)
svc_lin = SVC(C=1, cache_size=1000)
svc_rbf = SVC(C=100, gamma=0.001, kernel='rbf', cache_size=1000)
lr = LogisticRegression()
xgb = XGBClassifier(n_estimators=1000, learning_rate=0.3)
"""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Choose and train classifier
clf = bdt 
#clf.fit(x_train[:500], y_train[:500]) # work with subset
clf.fit(x_train, y_train)

y_test_predicted = clf.predict(x_test)
y_test_scores = clf.decision_function(x_test)
print("BDT Accuracy: %.2f%%" % ((accuracy_score(y_test,y_test_predicted))* 100.0))
"""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Plot ROC curve
from sklearn.metrics import roc_curve, auc

def roc_auc(y, y_score):
    fpr, tpr, _ = roc_curve(y, y_score)
    roc_auc = auc(fpr, tpr)
    print("BDT ROC_AUC: %.2f%%" % ((roc_auc)* 100.0))
    return roc_auc

def plot_roc(y, y_score):
    fpr, tpr, _ = roc_curve(y, y_score)
    rocauc = auc(fpr, tpr)
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',lw=lw,
             label='ROC curve (area = %0.3f)'%rocauc)
    plt.plot([0,1],[0,1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.05])
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('Baseline comparison: ROC for BDT score')
    plt.legend(loc="lower right")
    plt.savefig('ROC_Curve_sklearn.png')
    plt.show()
    return roc_auc
"""
roc_auc(y_test, y_test_scores)
plot_roc(y_test, y_test_scores)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Plot classifier
Classifier_training_S = clf.decision_function(x_train[y_train>0.5]).ravel()
Classifier_training_B = clf.decision_function(x_train[y_train<0.5]).ravel()
Classifier_testing_S = clf.decision_function(x_test[y_test>0.5]).ravel()
Classifier_testing_B = clf.decision_function(x_test[y_test<0.5]).ravel()

c_max = 12.0
c_min = -20.0
nbins = 50

Histo_training_S = np.histogram(Classifier_training_S,bins=nbins,range=(c_min,c_max),density=True)
Histo_training_B = np.histogram(Classifier_training_B,bins=nbins,range=(c_min,c_max),density=True)
Histo_testing_S = np.histogram(Classifier_testing_S,bins=nbins,range=(c_min,c_max),density=True)
Histo_testing_B = np.histogram(Classifier_testing_B,bins=nbins,range=(c_min,c_max),density=True)
# density=True normalises the histograms

AllHistos = [Histo_training_S,Histo_training_B,Histo_testing_S,Histo_testing_B]
h_max=max([histo[0].max() for histo in AllHistos])*1.2
h_min=max([histo[0].min() for histo in AllHistos])

bin_edges = Histo_training_S[1]
bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.0
bin_widths = (bin_edges[1:] - bin_edges[:-1])

ErrorBar_testing_S = np.sqrt(Histo_testing_S[0])
ErrorBar_testing_B = np.sqrt(Histo_testing_B[0])

plt.figure(figsize=(9,6))
ax1 = plt.subplot(111)

# Draw solid histograms for the training data
ax1.bar(bin_centers-bin_widths/2.,Histo_training_S[0],facecolor='blue',linewidth=0,width=bin_widths,label='S (Train)',alpha=0.5)
ax1.bar(bin_centers-bin_widths/2.,Histo_training_B[0],facecolor='red',linewidth=0,width=bin_widths,label='B (Train)',alpha=0.5)

# Draw error-bar histograms for the testing data
ax1.errorbar(bin_centers, Histo_testing_S[0], yerr=ErrorBar_testing_S, xerr=None, ecolor='blue',c='blue',fmt='o',label='S (Test)')
ax1.errorbar(bin_centers, Histo_testing_B[0], yerr=ErrorBar_testing_B, xerr=None, ecolor='red',c='red',fmt='o',label='B (Test)')

# Make a colorful backdrop to show the clasification regions in red and blue
middle = 0.0
ax1.axvspan(middle, c_max, color='blue',alpha=0.08)
ax1.axvspan(c_min,middle, color='red',alpha=0.08)
 
# Adjust the axis boundaries (just cosmetic)
ax1.axis([c_min, c_max, h_min, h_max])
 
# Make labels and title
plt.title("Classification with scikit-learn")
plt.xlabel("Classifier, BDT[n_estimators=500,learning_rate=0.25,max_leaf_nodes=20,max_features=None]")
plt.ylabel("Counts/Bin")
 
# Make legend with smalll font
legend = ax1.legend(loc='upper center', shadow=True,ncol=2)
for alabel in legend.get_texts():
    alabel.set_fontsize('small')
                       
# Save the result to png
plt.savefig("Sklearn_BDT.png")
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""
# Try using XGBoost algorithm instead
model = xgb
print(model)
model.fit(x_train, y_train)
y_pred = model.predict(x_test)
predictions = [round(value,1) for value in y_pred]
accuracy = accuracy_score(y_test, predictions)
print ("XBG Accuracy: %.2f%%" % (accuracy * 100.0))
print ("XGB ROC score: %.2f%%" % (roc_auc_score(y_test,predictions) * 100.0))

