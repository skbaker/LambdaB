## Script to test running locally.
## Was getting errors related to not finding the PV of Lb candidates. Solved by
## adding the line DaVinci().RootInTES = "/Event/Bhadron". This gives it the
## path so it knows where to look for PV
##
## Running DaVinci over data, for the normalisation channel, Lb -> Lc (-> pKpi) pi
##
## Script very similar to script to run over MC
##
## The input and output will be defined by Ganga.
## Name of ROOT output file needs to match the filename in ganga_DaVinci.py
##
################################
################################
# Things need to be re-written to run on batch
Name       = "Data_Lb_Lcpi"
datatype   = "2016"
abs_path   = "/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/Tuples"
#jnumber    = "JOBNUMBER"
#filename   = "PATHTOFILE"
#mag        = "MAGNET"
################################
################################
## For running locally: (with one input file)
jnumber    = "001"
filename   = "/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/DST/"\
             "00061346_00024478_1.bhadron.mdst"
mag        = "mu"
################################
################################

from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import LoKi__HDRFilter

#------------------------------------------------------------------------------
## Check for primary vertex, so TupleToolPropertime works
## --- this did not solve the problem; see above for solution.
#from Configurables import CheckPV, GaudiSequencer
#checkPV = CheckPV("onePV")
#checkPV.MinPVs = 1

#MySequencer = GaudiSequencer('Sequence')
#MySequencer.Members = [checkPV]

#------------------------------------------------------------------------------
# Fill tuple
from Configurables import ( DecayTreeTuple
                          , EventTuple
                          , TupleToolTrigger
                          , TupleToolTISTOS
                          , FilterDesktop
                          , TupleToolStripping
                          , TupleToolKinematic
                          )
from Configurables import ( BackgroundCategory
                          , TupleToolDecay
                          , TupleToolVtxIsoln
                          , TupleToolPid
                          , EventCountHisto
                          , TupleToolRecoStats
                          )

from Configurables import TupleToolDecayTreeFitter
from Configurables import LoKi__Hybrid__TupleTool
from DecayTreeTuple.Configuration import *

tuple = DecayTreeTuple("Lb_Lcpi_Tuple")

tuple.Inputs = ["/Event/Bhadron/Phys/Lb2LcPiLc2PKPiBeauty2CharmLine/Particles"]

tuple.Decay = "[Lambda_b0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^pi-]CC"

tuple.addBranches({
                   "Lambda_b":"[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi-]CC"
                 , "Lambda_c":"[Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) pi-]CC"
                 , "p"       :"[Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) pi-]CC"
                 , "K"       :"[Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) pi-]CC"
                 , "piplus"  :"[(Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) pi-),"\
                               "(Lambda_b~0 -> (Lambda_c~- -> p~- K+ pi-) ^pi+)]"
                 , "piminus" :"[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^pi-),"\
                               "(Lambda_b~0 -> (Lambda_c~- -> p~- K+ ^pi-) pi+)]"
                 })


for particle in ["Lambda_c","p","K","piplus","piminus"]:
    tuple.addTool(TupleToolDecay, name = particle)

tuple.ToolList += [ "TupleToolAngles"
                  , "TupleToolEventInfo"
                  , "TupleToolGeometry"
                  , "TupleToolKinematic"
                  , "TupleToolPid"
                  , "TupleToolPrimaries"
                  , "TupleToolPropertime"
                  , "TupleToolTrackInfo"
                  ]

tuple.addTool(TupleToolPid, name="TupleToolPid")
tuple.TupleToolPid.Verbose = True

## Reference point
tuple.ToolList+=[ "TupleToolKinematic" ]
tuple.addTool(TupleToolKinematic, name="TupleToolKinematic")
tuple.TupleToolKinematic.Verbose = True

## Triggers
_trigger_list = [
    #L0
     'L0HadronDecision'
    ,'L0ElectronDecision'
    ,'L0ElectronHiDecision'
    ,'L0MuonDecision'
    ,'L0DiMuonDecision'
    ,'L0MuonHighDecision'
    ,'L0PhotonDecision'
    #Hlt1
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonNoIPDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1MultiMuonNoL0Decision'
    ,'Hlt1CalibMuonAlignJpsiDecision'
    ,'Hlt1DiMuonNoL0Decision'
    ,'Hlt1TrackMVADecision'
    ,'Hlt1TwoTrackMVADecision'
    ,'Hlt1B2GammaGammaDecision'
    ,'Hlt1B2HH_LTUNB_KKDecision'
    ,'Hlt1B2HH_LTUNB_KPiDecision'
    ,'Hlt1B2HH_LTUNB_PiPiDecision'
    ,'Hlt1B2PhiGamma_LTUNBDecision'
    ,'Hlt1B2PhiPhi_LTUNBDecision'
    ,'Hlt1BeamGasBeam1Decision'
    ,'Hlt1BeamGasBeam2Decision'
    ,'Hlt1BeamGasCrossingEnhancedBeam1Decision'
    ,'Hlt1BeamGasCrossingEnhancedBeam2Decision'
    ,'Hlt1BeamGasCrossingForcedRecoDecision'
    ,'Hlt1BeamGasCrossingForcedRecoFullZDecision'
    ,'Hlt1BeamGasHighRhoVerticesDecision'
    ,'Hlt1BeamGasNoBeamBeam1Decision'
    ,'Hlt1BeamGasNoBeamBeam2Decision'
    ,'Hlt1CEPDecision'
    ,'Hlt1CEPVeloCutDecision'
    ,'Hlt1CalibHighPTLowMultTrksDecision'
    ,'Hlt1CalibRICHMirrorRICH1Decision'
    ,'Hlt1CalibRICHMirrorRICH2Decision'
    ,'Hlt1CalibTrackingKKDecision'
    ,'Hlt1CalibTrackingKPiDecision'
    ,'Hlt1CalibTrackingKPiDetachedDecision'
    ,'Hlt1CalibTrackingPiPiDecision'
    #,'Hlt1DiProtonDecision'
    #,'Hlt1DiProtonLowMultDecision'
    ,'Hlt1IncPhiDecision'
    ,'Hlt1L0AnyDecision'
    ,'Hlt1L0AnyNoSPDDecision'
    ,'Hlt1LumiDecision'
    ,'Hlt1MBNoBiasDecision'
    ,'Hlt1MBNoBiasRateLimitedDecision'
    ,'Hlt1NoBiasNonBeamBeamDecision'
    ,'Hlt1NoPVPassThroughDecision'
    ,'Hlt1ODINTechnicalDecision'
    ,'Hlt1SingleElectronNoIPDecision'
    ,'Hlt1Tell1ErrorDecision'
    ,'Hlt1TrackMuonNoSPDDecision'
    ,'Hlt1VeloClosingMicroBiasDecision'
    ,'Hlt1BeamGasCrossingParasiticDecision'
    ,'Hlt1ErrorEventDecision'
    ,'Hlt1GlobalDecision'
    #Hlt2
    ,'Hlt2DebugEventDecision'
    ,'Hlt2DiMuonBDecision'
    ,'Hlt2DiMuonBTurboDecision'
    ,'Hlt2DiMuonBTurboDecision'
    ,'Hlt2DiMuonDetachedDecision'
    ,'Hlt2DiMuonDetachedHeavyDecision'
    ,'Hlt2DiMuonDetachedJPsiDecision'
    ,'Hlt2DiMuonDetachedPsi2SDecision'
    ,'Hlt2DiMuonJPsiDecision'
    ,'Hlt2DiMuonJPsiHighPTDecision'
    ,'Hlt2DiMuonJPsiTurboDecision'
    ,'Hlt2DiMuonJPsiTurboDecision'
    ,'Hlt2DiMuonPsi2SDecision'
    ,'Hlt2DiMuonPsi2SHighPTDecision'
    ,'Hlt2DiMuonPsi2STurboDecision'
    ,'Hlt2DiMuonPsi2STurboDecision'
    ,'Hlt2DiMuonSoftDecision'
    ,'Hlt2DiMuonZDecision'
    ,'Hlt2EWDiElectronDYDecision'
    ,'Hlt2EWDiElectronHighMassDecision'
    ,'Hlt2EWDiMuonDY1Decision'
    ,'Hlt2EWDiMuonDY2Decision'
    ,'Hlt2EWDiMuonDY3Decision'
    ,'Hlt2EWDiMuonDY4Decision'
    ,'Hlt2EWDiMuonZDecision'
    ,'Hlt2EWSingleElectronHighPtDecision'
    ,'Hlt2EWSingleElectronLowPtDecision'
    ,'Hlt2EWSingleElectronVHighPtDecision'
    ,'Hlt2EWSingleMuonHighPtDecision'
    ,'Hlt2EWSingleMuonLowPtDecision'
    ,'Hlt2EWSingleMuonVHighPtDecision'
    ,'Hlt2EWSingleTauHighPt2ProngDecision'
    ,'Hlt2EWSingleTauHighPt3ProngDecision'
    ,'Hlt2ForwardDecision'
    ,'Hlt2SingleMuonDecision'
    ,'Hlt2SingleMuonHighPTDecision'
    ,'Hlt2SingleMuonLowPTDecision'
    ,'Hlt2SingleMuonNoSPDDecision'
    ,'Hlt2SingleMuonRareDecision'
    ,'Hlt2SingleMuonVHighPTDecision'
    ,'Hlt2Topo2BodyDecision'
    ,'Hlt2Topo3BodyDecision'
    ,'Hlt2Topo4BodyDecision'
    ,'Hlt2TopoMu2BodyDecision'
    ,'Hlt2TopoMu3BodyDecision'
    ,'Hlt2TopoMu4BodyDecision'
    ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalibDecision'
    ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalibDecision'
    ,'Hlt2TransparentDecision'
    ,'Hlt2ErrorEventDecision'
    ,'Hlt2GlobalDecision'
    ]

tuple.ToolList+=[ "TupleToolTISTOS" ]
tuple.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
tuple.TupleToolTISTOS.Verbose = True
tuple.TupleToolTISTOS.VerboseL0 = True
tuple.TupleToolTISTOS.VerboseHlt1 = True
tuple.TupleToolTISTOS.VerboseHlt2 = True
tuple.TupleToolTISTOS.TriggerList = _trigger_list #trigger_list.trigger_list()

from Configurables import TupleToolRecoStats
tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
tuple.TupleToolRecoStats.Verbose=True

LoKi_All = tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = { 'ETA' : 'ETA' }

#------------------------------------------------------------------------------
# Input
## The input is configured automatically when submitted through Ganga, so these
## lines are not needed
from GaudiConf import IOHelper
IOHelper().inputFiles( [ filename ] , clear=True )

#------------------------------------------------------------------------------
# DaVinci Configuration

from Configurables import DaVinci
DaVinci().InputType = "MDST"
DaVinci().DataType = datatype
DaVinci().Simulation = False 
DaVinci().Lumi = True
#DaVinci().EvtMax = -1
DaVinci().EvtMax = 5000 
## Require this line for it to find PVs of candidates
DaVinci().RootInTES = "/Event/Bhadron"
## Ignore histogram files for Ganga submission
DaVinci().HistogramFile = abs_path+"/DVHistos_"+mag+"_"+jnumber+".root"
DaVinci().TupleFile = abs_path+"/Data_Lb_LcPi_2016.root" 
DaVinci().UserAlgorithms  = [ tuple ]
#DaVinci().appendToMainSequence( [ MySequencer, tuple ])

# database
## Conditions taken from bookkeeping
DaVinci().DDDBtag   = "dddb-20150724" 
DaVinci().CondDBtag = "cond-20161004"
