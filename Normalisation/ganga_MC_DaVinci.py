#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Run DaVinci on the grid
## Script used to process DST files for 2012 MC into nTuples.
## Run a separate Ganga job for each collection of DSTs: MagUp/MagDown,
## Pythia 6/8
## ROOT nTuples must be merged afterwards
##
## Name of ROOT nTuples needs to match name in DaVinci script
##
## Run as: $ ganga ganga_DaVinci.py
## 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Sumbmission functions copied from Matt:
def sendJob(filename, dataset, name):
    j = Job()
    j.application = DaVinci(version='v41r1')
    j.application.optsfile = [ filename ]
    j.backend = Dirac()
    j.inputdata = dataset
    j.splitter = SplitByFiles(filesPerJob=3)
    j.outputfiles = [ DiracFile('MC_Lb_LcPi_2016.root') ]
    j.parallel_submit = True
    j.name = name
    j.submit()

def getFromBookkeeping(datapath):
    bkk = BKQuery(datapath)
    data = bkk.getDataset()
    return data

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Try submitting job using Matt's method
#firstDataSetPath = '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15264011/ALLSTREAMS.DST'
#firstDataSetPath = '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15264011/ALLSTREAMS.DST'
#firstDataSetPath = '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia6/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15264011/ALLSTREAMS.DST'
#firstDataSetPath = '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15264011/ALLSTREAMS.DST'
#firstDataSetPath = '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15364010/ALLSTREAMS.MDST'
firstDataSetPath = '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/15364010/ALLSTREAMS.MDST'

Data = getFromBookkeeping(firstDataSetPath)

## Modify so that Data is only one file, for trial
## REMOVE this line to submit the full dataset
Data.files = Data.files[0:1]
print Data

sendJob('DaVinci_Lb_Lcpi_MC_selection.py', Data, firstDataSetPath)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
