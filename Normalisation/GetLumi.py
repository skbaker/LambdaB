## Quick script to find integrated luminosity of data Tuple
##
## The resulting error in luminosity is identical to the result for the
## integrated luminosity. Not sure what is happening in nTuple to give that.
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import ROOT

f = ROOT.TFile.Open('/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/Tuples/'\
                    'Data_Lb_LcPi_2016.root')
t = f.Get('GetIntegratedLuminosity/LumiTuple')

lumi = []
lumi_err = []
for entry in t:
    lumi.append(t.IntegratedLuminosity)
    lumi_err.append(t.IntegratedLuminosityErr)

tot_lumi = sum(lumi)
tot_lumi_err = sum(lumi_err)

print('Total integrated luminosity: {0:.2f} +/- {1:.2f}'\
      ' pb^-1'.format(tot_lumi,tot_lumi_err))
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
