#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Run as `lb-run Bender/latest python -i first.py NameOf.dst >> Output.txt`
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
### Script from Starterkit to 'interactively explore a DST'

import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2012'

# Pass file to open as first command line argument
inputFiles = [sys.argv[-1]]
IOHelper('ROOT').inputFiles(inputFiles)

appMgr = GP.AppMgr()
evt = appMgr.evtsvc()

appMgr.run(1)
evt.dump()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Print interesting TES locations
def nodes(evt, node=None):
    """List all nodes in `evt`"""
    nodenames = []

    if node is None:
        root = evt.retrieveObject('')
        node = root.registry()

    if node.object():
        nodenames.append(node.identifier())
        for l in evt.leaves(node):
            # skip a location that takes forever to load
            # XXX How to detect these automatically??
            if 'Swum' in l.identifier():
                continue
            
            temp = evt[l.identifier()]
            nodenames += nodes(evt, l)
    else:
        nodenames.append(node.identifier())
    
    return nodenames

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Inspect particles and vertices in stripping line
def advance(decision):
    """Advance until stripping decision is true, returns number of events
    by which we advanced"""
    n = 0
    while True:
        appMgr.run(1)

        if not evt['/Event/Rec/Header']:
            print 'Reached end of input files'
            break

        n += 1
        dec=evt['/Event/Strip/Phys/DecReports']
        if dec.hasDecisionName('Stripping{0}Decision'.format(decision)):
            break

    return n

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Advance through DST until an event containing the stripping candidate is
## reached. Can only be used if stripping line is known.
line = 'Lb2LcPiLc2PKPiBeauty2CharmLine'
advance(line)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Prints out all of the stripping lines in the events in the DST.
## Pipe to DecReports.txt
while appMgr.run(1):
    dec = evt['/Event/Strip/Phys/DecReports']
    print dec.decReports()
