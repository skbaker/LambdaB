# Partially reconsructed backgrounds
There is **no** MC for the part-reco backgrounds. Instead, follow the strategy
in LHcb-ANA-2016-030 and fit an Argus(X)Gauss function directly to the
background to model the shoulder.  

Directory for work regarding the part reco backgrounds for the normalisation
channel.  
These are **Lb -> (SigmaC -> Lc pi0) pi** and **Lb -> Lc (rho -> pi pi0)**.  


Process MC and then use for fit in signal.

### first.py
An attempt to find the stripping line that contains the data for
**Lb -> SigmaC pi**. Output found in DecReports.txt.  
Maybe further investigation of the other stripping lines will lead to it?  
DST files are in `/eos/lhcb/user/s/skbaker/Lb_Lcpi/PartReco`.
