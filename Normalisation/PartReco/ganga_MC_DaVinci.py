#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Run DaVinci on the grid
## Script used to process DST files for 2012 MC into nTuples.
## Run a separate Ganga job for each collection of DSTs: MagUp/MagDown,
## Pythia 6/8
## ROOT nTuples must be merged afterwards
##
## Name of ROOT nTuples needs to match name in DaVinci script
##
## Run as: $ ganga ganga_DaVinci.py
## 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Sumbmission functions copied from Matt:
def sendJob(filename, dataset, name):
    j = Job()
    j.application = DaVinci(version='v41r1')
    j.application.optsfile = [ filename ]
    j.backend = Dirac()
    j.inputdata = dataset
    j.splitter = SplitByFiles(filesPerJob=3)
    j.outputfiles = [ DiracFile('MC_Lb_SigmacPi_2012.root') ]
    j.parallel_submit = True
    j.name = name
    j.submit()

def getFromBookkeeping(datapath):
    bkk = BKQuery(datapath)
    data = bkk.getDataset()
    return data

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Try submitting job using Matt's method
firstDatasetPath = '/lhcb/MC/2012/ALLSTREAMS.DST/00035315/0000/'\
                   '00035315_00000001_1.allstreams.dst' 

Data = getFromBookkeeping(firstDatasetPath)

## Modify so that Data is only one file, for trial
## REMOVE this line to submit the full dataset
Data.files = Data.files[0:1]
print Data

sendJob('DaVinci_Lb_Sigmacpi_MC_selection.py', Data, firstDatasetPath)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
