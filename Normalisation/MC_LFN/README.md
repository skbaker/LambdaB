# MC data for normalisation channel

Only use the DSTs which are Sim08g. The other ones (Sim08d) do not contain all of the
stripping passes and tags etc. Don't know why?!

### DST\_MagDown/Up.py
List of DST files to process into nTuples with DaVinci.  
Both Pythia 6 and 8 DST files for MagUp and MagDown, with Sim08g configuration, from MC201215264011Beam4000GeV-2012\*\*\*.py files.
