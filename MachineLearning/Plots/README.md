# Machine Learning
Plots from `TrainSKLearn.py` with k-folding.

![](ROC_Curve_sklearn_0.png)
![](ROC_Curve_sklearn_1.png)
![](ROC_Curve_sklearn_2.png)
![](ROC_Curve_sklearn_3.png)
![](ROC_Curve_sklearn_4.png)
![](ROC_Curve_sklearn_5.png)
![](ROC_Curve_sklearn_6.png)
![](ROC_Curve_sklearn_7.png)
![](Sklearn_BDT_0.png)
![](Sklearn_BDT_1.png)
![](Sklearn_BDT_2.png)
![](Sklearn_BDT_3.png)
![](Sklearn_BDT_4.png)
![](Sklearn_BDT_5.png)
![](Sklearn_BDT_6.png)
![](Sklearn_BDT_7.png)
