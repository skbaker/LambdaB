# Script first tested in SWAN notebook.
# Follow guidance and example by Andrew
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import sklearn
from sklearn.model_selection import KFold, train_test_split
from sklearn.model_selection import GridSearchCV 
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, auc, roc_auc_score

from xgboost.sklearn import XGBClassifier
from sklearn.datasets import load_digits 

import numpy as np
import root_numpy
from root_numpy import root2array, rec2array

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

# Discriminating variables
## Try using these when they are transformed?
## i.e. the same as in TMVA
straightvariables = ["Lambda_b_DIRA_OWNPV"
            ,"Lambda_b_TAU"
            ,"Lambda_b_IPCHI2_OWNPV"
            ,"Lambda_b_FDCHI2_OWNPV"
            ,"Lambda_b_ENDVERTEX_CHI2"
            ,"Lambda_b_ETA"
            ,"Lambda_b_PT"
            ,"p_PT"
            ,"p_IPCHI2_OWNPV"
            ,"p_TRACK_CHI2NDOF"
            ,"deut_PT"
            ,"deut_IPCHI2_OWNPV"
            ,"deut_TRACK_CHI2NDOF"
            ]
scaledvariables = ["(1 - (1 - Lambda_b_DIRA_OWNPV)^(1/4))"
            ,"log(Lambda_b_TAU)"
            ,"log(Lambda_b_IPCHI2_OWNPV)"
            ,"log(Lambda_b_FDCHI2_OWNPV)"
            ,"log(Lambda_b_ENDVERTEX_CHI2)"
            #,"Lambda_b_ETA"
            #,"Lambda_b_PT"
            #,"p_PT"
            ,"log(p_IPCHI2_OWNPV)"
            ,"p_TRACK_CHI2NDOF"
            #,"deut_PT"
            ,"log(deut_IPCHI2_OWNPV)"
            ,"deut_TRACK_CHI2NDOF"
            ]

#variables = straightvariables
variables = scaledvariables

File = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/TMVA/'\
       'training_samples_Lb_dp_bothSidebands.root'

signal = root2array(File,'SignalTree',variables)
signal = rec2array(signal)
background = root2array(File,'BkgTree',variables)
background = rec2array(background)

# Force signal and background samples to be same size
background = background[:len(signal)]

# x = matrix of feature values, y = array of target values
x = np.concatenate((signal,background))
y = np.concatenate((np.ones(signal.shape[0]),np.zeros(background.shape[0])))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Define classifiers
bdt = GradientBoostingClassifier(n_estimators=100, learning_rate=0.4,\
                      max_leaf_nodes=20, max_features=None, verbose=1)
abc = AdaBoostClassifier(n_estimators=1000, learning_rate=0.3)
rf = RandomForestClassifier(n_estimators=5000,max_leaf_nodes=100,n_jobs=-1)
svc_lin = SVC(C=1, cache_size=1000)
svc_rbf = SVC(C=100, gamma=0.001, kernel='rbf', cache_size=1000)
lr = LogisticRegression()
xgb = XGBClassifier(n_estimators=50, learning_rate=0.25,silent=True,
max_depth=3)
## Higher max depth is better, I think

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Plot ROC curve functions
from sklearn.metrics import roc_curve, auc

def roc_auc(y, y_score):
    fpr, tpr, _ = roc_curve(y, y_score)
    roc_auc = auc(fpr, tpr)
    print("BDT ROC_AUC: %.2f%%" % ((roc_auc)* 100.0))
    return roc_auc

def plot_roc(y, y_score, INDEX):
    fpr, tpr, _ = roc_curve(y, y_score)
    rocauc = auc(fpr, tpr)
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',lw=lw,
             label='ROC curve (area = %0.3f)'%rocauc)
    plt.plot([0,1],[0,1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0,1.0])
    plt.ylim([0.0,1.05])
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('Baseline comparison: ROC for BDT score')
    plt.legend(loc="lower right")
    plt.savefig('Plots/ROC_Curve_sklearn_{0}.png'.format(INDEX))
    plt.show()
    return roc_auc

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Plot signal and background 
def plot_sigbkg(XTRAIN,YTRAIN,XTEST,YTEST,MODEL,INDEX):
    Classifier_training_S = MODEL.decision_function(XTRAIN[YTRAIN>0.5]).ravel()
    Classifier_training_B = MODEL.decision_function(XTRAIN[YTRAIN<0.5]).ravel()
    Classifier_testing_S = MODEL.decision_function(XTEST[YTEST>0.5]).ravel()
    Classifier_testing_B = MODEL.decision_function(XTEST[YTEST<0.5]).ravel()

    c_max = 6.0
    c_min = -8.0
    nbins = 56

    Histo_training_S = np.histogram(Classifier_training_S,bins=nbins,
    range=(c_min,c_max),density=True)
    Histo_training_B = np.histogram(Classifier_training_B,bins=nbins,
    range=(c_min,c_max),density=True)
    Histo_testing_S = np.histogram(Classifier_testing_S,bins=nbins,
    range=(c_min,c_max),density=True)
    Histo_testing_B = np.histogram(Classifier_testing_B,bins=nbins,
    range=(c_min,c_max),density=True)
    # density=True normalises the histograms

    AllHistos = [Histo_training_S,Histo_training_B,Histo_testing_S,Histo_testing_B]
    h_max=max([histo[0].max() for histo in AllHistos])*1.2
    h_min=max([histo[0].min() for histo in AllHistos])

    bin_edges = Histo_training_S[1]
    bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.0
    bin_widths = (bin_edges[1:] - bin_edges[:-1])

    ## Need to fix these errors. They need scaling somehow.
    ErrorBar_testing_S = (np.sqrt(Histo_testing_S[0]))#*\
                         #(Histo_training_S[0]/Histo_testing_S[0])
    ErrorBar_testing_B = np.sqrt(Histo_testing_B[0])


    plt.figure(figsize=(9,6))
    ax1 = plt.subplot(111)

    # Draw solid histograms for the training data
    ax1.bar(bin_centers-bin_widths/2.,Histo_training_S[0],facecolor='blue',
    linewidth=0,width=bin_widths,label='S (Train)',alpha=0.5)
    ax1.bar(bin_centers-bin_widths/2.,Histo_training_B[0],facecolor='red',
    linewidth=0,width=bin_widths,label='B (Train)',alpha=0.5)

    # Draw error-bar histograms for the testing data
    ax1.errorbar(bin_centers, Histo_testing_S[0], yerr=ErrorBar_testing_S,
    xerr=None, ecolor='blue',c='blue',fmt='o',label='S (Test)')
    ax1.errorbar(bin_centers, Histo_testing_B[0], yerr=ErrorBar_testing_B, 
    xerr=None, ecolor='red',c='red',fmt='o',label='B (Test)')

    # Make a colorful backdrop to show the clasification regions in red and blue
    middle = 0.0
    ax1.axvspan(middle, c_max, color='blue',alpha=0.08)
    ax1.axvspan(c_min,middle, color='red',alpha=0.08)
 
    # Adjust the axis boundaries (just cosmetic)
    ax1.axis([c_min, c_max, h_min, h_max])
 
    # Make labels and title
    plt.title("Classification with scikit-learn")
    plt.xlabel("Classifier, BDT[n_estimators=500,learning_rate=0.25,"\
    "max_leaf_nodes=20,max_features=None]")
    plt.ylabel("Counts/Bin")
 
    # Make legend with smalll font
    legend = ax1.legend(loc='upper center', shadow=True,ncol=2)
    for alabel in legend.get_texts():
        alabel.set_fontsize('small')
                       
    # Save the result to png
    plt.savefig("Plots/Sklearn_BDT_{0}.png".format(INDEX))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Choose and train classifier
clf = bdt 
#clf = xgb 
#clf.fit(x_train[:500], y_train[:500]) # work with subset
#clf.fit(x_train, y_train)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Split into training and test sets
#x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.2,random_state=42)

## Try k-folding
kf = KFold(n_splits=2,shuffle=True,random_state=4) 
# Add 'indices=True' if you need to keep the original data, rather than the elements
kf.get_n_splits(x)

print(kf)

for k, (train_index, test_index) in enumerate(kf.split(x,y)):
    print 'Next fold'
    print("TRAIN:", train_index, "TEST:", test_index)
    x_train, x_test =  x[train_index], x[test_index] 
    y_train, y_test =  y[train_index], y[test_index] 
  
    if(clf!=xgb):
        clf.fit(x_train, y_train)
        y_test_predicted = clf.predict(x_test)
        y_test_scores = clf.decision_function(x_test)
        print("BDT Accuracy: %.2f%%" % 
              ((accuracy_score(y_test,y_test_predicted))*100.0))
        print y_test
        print y_test_scores
        roc_auc(y_test, y_test_scores)
        print "Plotting ROC"
        plot_roc(y_test, y_test_scores, k)
        print "Plotting sig bkg"
        plot_sigbkg(x_train,y_train,x_test,y_test,clf,k)

    if(clf==xgb):
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)
        predictions = [round(value,1) for value in y_pred]
        accuracy = accuracy_score(y_test, predictions)
        print ("XBG Accuracy: %.2f%%" % (accuracy * 100.0))
        print ("XGB ROC score: %.2f%%" % (roc_auc_score(y_test,predictions)*100.0))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Try GridSearchCV -- try to maximise the separation
## Give GridSearch a range of values of each parameter to try
## How to score best values?
##
##bdt = GradientBoostingClassifier(n_estimators=100, learning_rate=0.4,\
##                      max_leaf_nodes=20, max_features=None, verbose=1)

#NEst = np.array([10,100]) 
Rate = np.array([0.1,0.5,1.0])
Node = np.array([6,8,10])
model = GridSearchCV(estimator=bdt,param_grid=[{'max_leaf_nodes':Node}],n_jobs=1)
x_train, x_test, y_train, y_test = train_test_split(x, y,
                                   test_size=0.33, random_state=42)
model.fit(x_train,y_train)
print 'Best score:'
print model.best_score_
print 'Best estimator, max_leaf_nodes:'
print model.best_estimator_.max_leaf_nodes
print 'Grid scores'
print model.cv_results_

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Try using XGBoost algorithm instead
## This algorithm is outperformed by a regular BDT, but perhaps the
## hyperparameters just need more work?

model = xgb
print(model)
model.fit(x_train, y_train)
y_pred = model.predict(x_test)
predictions = [round(value,1) for value in y_pred]
accuracy = accuracy_score(y_test, predictions)
print ("XBG Accuracy: %.2f%%" % (accuracy * 100.0))
print ("XGB ROC score: %.2f%%" % (roc_auc_score(y_test,predictions) * 100.0))

plot_roc(y_test, predictions)
## Plotting doesn't work for XGB (at least not using this method -- doesn't
## take decision_function)
plot_sigbkg(x_train,y_train,x_test,y_test,model)
"""
