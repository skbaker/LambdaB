from ROOT import *

### Create training samples
##############################
## Don't think this needs to be executed -- can just use the signal MC, and the 
## upper mass sideband directly from data.
## Can execute this at a later time, if necessary.
"""
## Select MC magnet polarity!
f_sig = TFile("/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/"\
              "MC_Lb_dppipi_2016_MagDown.root")
t_sig_all = f_sig.Get("Lb_dppipi_Tuple/DecayTree")

f_bkg = TFile("/eos/lhcb/user/s/skbaker/Lb_dppipi_Strip26/Data/"\
              "BLIND_Lb_dppipi_S26_2016_md.root")
t_bkg_all = f_bkg.Get("DecayTree")

fout = TFile("/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/TMVA/"\
             "training_samples_Lb_dppipi.root",\
             "recreate")
## Cut out backgrounds from (mis)reconstruction in MC 
t_sig = t_sig_all.CopyTree("Lambda_b_BKGCAT < 55")
t_sig.Write("SignalTree")
## Upper mass sideband and magnet polarity 
t_bkg = t_bkg_all.CopyTree("Lambda_b_M_deuthyp > 5770 && Lambda_b_M_deuthyp"\
                           " < 7000 && Polarity==-1")
t_bkg.Write("BkgTree")
"""
## Need to veto Lc mass in (p deut pi) mass, change deut -> K mass, and match
## ID sign of p and pi



### Define TMVA factory
##############################
## Select magnet polarity
fin = TFile('/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/TMVA/'\
            'training_samples_Lb_dppipi.root')

t_sig = fin.Get("SignalTree") 
t_bkg = fin.Get("BkgTree")

fout = TFile("training.root","recreate")

TMVA.Tools.Instance()

factory = TMVA.Factory('MyClassification', fout,
                            ":".join([
                            "!V",# verbose output
                            "!Silent",# if true, inhibits output
                            "Color",# coloured screen output
                            "DrawProgressBar",# displays schedule
                            "Transformations=I;D;P;G,D",# list of transformations
                            "AnalysisType=Classification"])# set analysis type
                            )

## Take discriminating variables from plots 
#factory.AddVariable("Lambda_b_OWNPV_CHI2","F")
factory.AddVariable("(1-(1-Lambda_b_DIRA_OWNPV)^(1/4))","F")
#factory.AddVariable("Lambda_b_TAU","F")
factory.AddVariable("log(Lambda_b_TAUCHI2)","F")
#factory.AddVariable("Lambda_b_IP_OWNPV","F")
factory.AddVariable("log(Lambda_b_IPCHI2_OWNPV)","F")
factory.AddVariable("log(Lambda_b_FDCHI2_OWNPV)","F")
factory.AddVariable("log(Lambda_b_ENDVERTEX_CHI2)","F")
#factory.AddVariable("Lambda_b_ETA","F")
#factory.AddVariable("Lambda_b_PT","F")
#factory.AddVariable("p_PT","F")
#factory.AddVariable("_PT","F")
factory.AddVariable("p_TRACK_CHI2NDOF","F")
factory.AddVariable("log(p_IPCHI2_OWNPV)","F")
factory.AddVariable("deut_TRACK_CHI2NDOF","F")
factory.AddVariable("log(deut_IPCHI2_OWNPV)","F")
factory.AddVariable("pi_plus_TRACK_CHI2NDOF","F")
factory.AddVariable("log(pi_plus_IPCHI2_OWNPV)","F")
factory.AddVariable("pi_minus_TRACK_CHI2NDOF","F")
factory.AddVariable("log(pi_minus_IPCHI2_OWNPV)","F")

## Weights can be added to these, for if you want multiple signal and bkg trees
factory.AddSignalTree(t_sig)
factory.AddBackgroundTree(t_bkg)

# cuts
## No cuts
Cut = TCut("1") 
## Cuts to make variable ranges 'physical'
myCut = TCut("Lambda_b_TAU > 0.0 && Lambda_b_TAUCHI2 > 0.0 && "\
             "Lambda_b_OWNPV_CHI2 > 0.0 && Lambda_b_IPCHI2_OWNPV > 0.0 &&"\
             "Lambda_b_DIRA_OWNPV > 0.999 && Lambda_b_TAU < 0.05 &&"\
             "Lambda_b_TAUCHI2 < 30.0 && Lambda_b_IPCHI2_OWNPV < 30.0")

factory.PrepareTrainingAndTestTree(myCut,
                                   #":".join(["nTrain_Signal=5000"
                                   #        , "nTrain_Background=5000"
                                   #        , "nTest_Signal=5000"
                                   #        , "nTest_Background=5000"
                                   ":".join(["nTrain_Signal=0"
                                           , "nTrain_Background=0"
                                           , "SplitMode=Random"
                                           , "NormMode=NumEvents"
                                           , "!V"
                                           ]))
# NormMode=NumEvents: Events in samples scaled to match nTrain_ amounts  

## Defining the classifiers to train
method = factory.BookMethod(TMVA.Types.kBDT, "BDT",
                            ":".join(["!H"
                                    , "!V"
                                    , "NTrees=1000"
                                   #, "MinNodeSize=0.5%"
                                    #, "MaxDepth=3"
                                    , "BoostType=Grad"#AdaBoost"
                                    , "AdaBoostBeta=0.5"
                                    , "SeparationType=GiniIndex"
                                    , "nCuts=20"
                                    , "PruneMethod=NoPruning"
                                    , "Shrinkage=0.4" # Learning rate
                                    ]))

### Traingin and testing
##############################
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

### Visualising the results
##############################
# TMVA.TMVAGUI("training.root")

