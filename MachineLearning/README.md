# Machine Learning

Directory for scripts relating to machine learning methods for signal/background
discrimination.  

Some scripts to use from Paula, sklearn from Andrew, mainly.  

Scripts are taken from pracice with the Lb -> Lcpi samples. Should be edited to
work for Lb -> dp instead.

Before trying to execute these files, need to configure the environment.  
For TMVA, need `SetupProject Urania v5r0`  
For SKLearn, need `source /cvmfs/sft.cern.ch/lcg/views/LCG_91/x86_64-slc6-gcc62-opt/setup.sh`, followed by `SetupProject Urania v5r0`   

### ROC Curve from sklearn
'Corner' in curve either means that the signal and background are very 
separable, or that there is a bug.  
Plot the sig and bkg histograms to be sure of what it looks like.

### K-folding
Trying this out in `TrainKFoldTMVA.py`.  
To maximise use of available signal and background data, want to k-fold it.  
Managed to get `HyperParameterOptimisation` and `CrossValidation` to run, but
need to be clear on what they are actually doing. What is the weights file that 
I end up with?  
For TMVA, to approaches that can be tried:
* [Cross-validation talk](https://indico.cern.ch/event/571102/contributions/2342484/attachments/1359710/2057400/CV_IML_Oct2016.pdf) -- Use `TMVA::HperParameterOptimisation`
* Follow Paula's code from Sam Hall's work. Self-made class to run TMVA and
k-fold the data, found on lx00 at `/home/hep/palvare1/public/Dendrology/`.
Edit and run `BDT_response.py` for your data.
* [Example code](https://indico.cern.ch/event/571102/contributions/2342484/attachments/1359802/2057386/Example_CrossValidation.C) for CrossValidation and 
HyperParameterOptimisation in TMVA


Also tried k-folding in sklearn. Plots below are ROC curves and separation for 
different folds.
