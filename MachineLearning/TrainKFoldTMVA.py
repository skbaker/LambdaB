## Copied from TrainTMVA.py, this script attempts to use k-folding to maximise
## use of available signal and background data
##
## Follow presentation https://indico.cern.ch/event/571102/contributions/2342484/attachments/1359710/2057400/CV_IML_Oct2016.pdf
## 
## Need to Setup Urania v5r0 AND source
##/cvmfs/sft.cern.ch/lcg/views/LCG_91/x86_64-slc6-gcc62-opt/setup.sh
## Otherwise not all TMVA packages will work
##
## Here the data is used with both sidebands, not just RHS.
## Also can use a subset of the training sets to get things off the ground.
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
from ROOT import *
from ROOT import TMVA

## Setup TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()

##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
fin = TFile('/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/TMVA/'\
                'training_mini_samples_Lb_dp_bothSidebands.root')
                #'training_samples_Lb_dp_bothSidebands.root')
t_sig = fin.Get("SignalTree") 
t_bkg = fin.Get("BkgTree")
fout = TFile("training.root","recreate")

## Use dataloader to import data, instead of factory
dataloader = TMVA.DataLoader('dataset')
## There is also a DataLoader.SplitSets() method available!

## Take discriminating variables from plots 
#dataloader.AddVariable("Lambda_b_OWNPV_CHI2","F")
dataloader.AddVariable("(1-(1-Lambda_b_DIRA_OWNPV)^(1/4))","F")
#dataloader.AddVariable("Lambda_b_TAU","F")
dataloader.AddVariable("log(Lambda_b_TAUCHI2)","F")
#dataloader.AddVariable("Lambda_b_IP_OWNPV","F")
dataloader.AddVariable("log(Lambda_b_IPCHI2_OWNPV)","F")
dataloader.AddVariable("log(Lambda_b_FDCHI2_OWNPV)","F")
dataloader.AddVariable("log(Lambda_b_ENDVERTEX_CHI2)","F")
#dataloader.AddVariable("Lambda_b_ETA","F")
#dataloader.AddVariable("Lambda_b_PT","F")
#dataloader.AddVariable("p_PT","F")
#dataloader.AddVariable("deut_PT","F")
dataloader.AddVariable("p_TRACK_CHI2NDOF","F")
dataloader.AddVariable("log(p_IPCHI2_OWNPV)","F")
dataloader.AddVariable("deut_TRACK_CHI2NDOF","F")
dataloader.AddVariable("log(deut_IPCHI2_OWNPV)","F")

## Weights can be added to these, for if you want multiple signal and bkg trees
dataloader.AddSignalTree(t_sig)
dataloader.AddBackgroundTree(t_bkg)

##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Cuts
## No cuts
Cut = TCut("1") 
## Cuts to make variable ranges 'physical'
myCut = TCut("Lambda_b_TAU > 0.0 && Lambda_b_TAUCHI2 > 0.0 && "\
             "Lambda_b_OWNPV_CHI2 > 0.0 && Lambda_b_IPCHI2_OWNPV > 0.0 &&"\
             "Lambda_b_DIRA_OWNPV > 0.999 && Lambda_b_TAU < 0.05 &&"\
             "Lambda_b_TAUCHI2 < 30.0 && Lambda_b_IPCHI2_OWNPV < 30.0")

##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
dataloader.PrepareTrainingAndTestTree(Cut,
                                   ":".join(["nTrain_Signal=500"
                                           , "nTrain_Background=500"
                                           , "SplitMode=Random"
                                           , "NormMode=NumEvents"
                                           , "!V"
                                           ]))
print 'Prepared training and testing trees'                                           
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Hyper Parameter Optimisation
HPO = TMVA.HyperParameterOptimisation(dataloader)

HPO.BookMethod(TMVA.Types.kBDT, "BDT",
                     ":".join(["!H"
                             , "!V"
                             , "NTrees=50"
                            #, "MinNodeSize=0.5%"
                             #, "MaxDepth=3"
                             , "BoostType=Grad"#AdaBoost"
                             , "AdaBoostBeta=0.5"
                             , "SeparationType=GiniIndex"
                             , "nCuts=20"
                             , "PruneMethod=NoPruning"
                             , "Shrinkage=0.4" # Learning rate
                             ]))

HPO.SetNumFolds(3)
HPO.SetFitter('Minuit')
HPO.SetFOMType('Separation')
print 'Evaluating HPO'
HPO.Evaluate()
HPOResult = HPO.GetResults()
HPOResult.Print()

##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Cross-Validation
print 'Cross-validating MVA'

CV = TMVA.CrossValidation(dataloader)

CV.BookMethod(TMVA.Types.kBDT, "BDT",
                     ":".join(["!H"
                             , "!V"
                             , "NTrees=50"
                            #, "MinNodeSize=0.5%"
                             #, "MaxDepth=3"
                             , "BoostType=Grad"#AdaBoost"
                             , "AdaBoostBeta=0.5"
                             , "SeparationType=GiniIndex"
                             , "nCuts=20"
                             , "PruneMethod=NoPruning"
                             , "Shrinkage=0.4" # Learning rate
                             ]))
CV.SetNumFolds(3)
CV.Evaluate()
CVResult = CV.GetResults()
## TMVA::CrossValidationResult -- should be able to extract TMultiGraph and
## plot the ROC curves on axes with range (0.9,1.0) instead of (0.0,1.0)
## And then save the results somewhere...
CVResult.Print()
CVResult.Draw()
"""
CVResult.GetTrainEff01Values()
CVResult.GetEff01Values()
CVResult.GetTrainEff10Values()
CVResult.GetEff10Values()
CVResult.GetTrainEff30Values()
CVResult.GetEff30Values()

print 'Eff. area values:'
print CVResult.GetEffAreaValues()
print 'ROC average'
print CVResult.GetROCAverage()
"""
## AllGraphs is a TMultiGraph
#AllGraphs = CVResult.GetROCCurves()
#AllGraphs.Print()
## Get histogram used to draw axis, and change max and min
#hist = AllGraphs.GetHistogram()
#hist.GetXaxis().SetRangeUser(0.8,1.0)
#hist.GetYaxis().SetRangeUser(0.8,1.0)
#AllGraphs.Draw()

"""
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Factory
factory = TMVA.Factory('MyClassification', fout,
                      ":".join([
                      "!V",# verbose output
                      "!Silent",# if true, inhibits output
                      "Color",# coloured screen output
                      "DrawProgressBar",# displays schedule
                      "Transformations=I;D;P;G,D",# list of transformations
                      "AnalysisType=Classification"])# set analysis type
                      )

factory.BookMethod(dataloader, TMVA.Types.kBDT, "BDT",
                     ":".join(["!H"
                             , "!V"
                             , "NTrees=100"
                            #, "MinNodeSize=0.5%"
                             #, "MaxDepth=3"
                             , "BoostType=Grad"#AdaBoost"
                             , "AdaBoostBeta=0.5"
                             , "SeparationType=GiniIndex"
                             , "nCuts=20"
                             , "PruneMethod=NoPruning"
                             , "Shrinkage=0.4" # Learning rate
                             ]))
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()
"""
"""
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Previous code 
#dataloader.PrepareTrainingAndTestTree(Cut,
#                                   ":".join(["nTrain_Signal=500"
#                                           , "nTrain_Background=500"
#                                           , "SplitMode=Random"
#                                           , "NormMode=NumEvents"
#                                           , "!V"
#                                           ]))


## Old code below, from TrainTMVA.py


factory = TMVA.Factory('MyClassification', fout,
                      ":".join([
                      "!V",# verbose output
                      "!Silent",# if true, inhibits output
                      "Color",# coloured screen output
                      "DrawProgressBar",# displays schedule
                      "Transformations=I;D;P;G,D",# list of transformations
                      "AnalysisType=Classification"])# set analysis type
                      )


# NormMode=NumEvents: Events in samples scaled to match nTrain_ amounts  

## Defining the classifiers to train
method = factory.BookMethod(TMVA.Types.kBDT, "BDT",
                            ":".join(["!H"
                                    , "!V"
                                    , "NTrees=1000"
                                   #, "MinNodeSize=0.5%"
                                    #, "MaxDepth=3"
                                    , "BoostType=Grad"#AdaBoost"
                                    , "AdaBoostBeta=0.5"
                                    , "SeparationType=GiniIndex"
                                    , "nCuts=20"
                                    , "PruneMethod=NoPruning"
                                    , "Shrinkage=0.4" # Learning rate
                                    ]))

### Training and testing
##############################
#factory.TrainAllMethods()
#factory.TestAllMethods()
#factory.EvaluateAllMethods()

### Visualising the results
##############################
# TMVA.TMVAGUI("training.root")
"""
