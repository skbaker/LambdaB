from ROOT import *
import array


reader = TMVA.Reader()

Var_names = ['B_plus_ENDVERTEX_CHI2',
             'B_plus_FDCHI2_OWNPV',
             'B_plus_DIRA_OWNPV']

Vars = {}
for varname in Var_names: Vars[varname] = array.array('f',[0])

reader.AddVariable("B_plus_ENDVERTEX_CHI2",Vars['B_plus_ENDVERTEX_CHI2'])
reader.AddVariable("B_plus_FDCHI2_OWNPV",Vars['B_plus_FDCHI2_OWNPV'])
reader.AddVariable("B_plus_DIRA_OWNPV",Vars['B_plus_DIRA_OWNPV'])

reader.BookMVA("BDT","weights/MyClassification_BDT.weights.xml")

# fin = TFile("/vols/lhcb/palvare1/B2pphgamma/B2ppKgamma/B2pphgamma_S21_protonID.root") ## We probably want to cut this a bit
fin = TFile("/vols/lhcb/palvare1/B2pphgamma/B2ppKgamma_MC/B2ppKgamma_S21.root")
tin= fin.Get("B2pphgamma_Tuple/DecayTree")

BDT = array.array('f',[0])

fout = TFile('/vols/lhcb/palvare1/B2pphgamma/B2ppKgamma/B2pphgamma_S21_protonID_BDT.root',"RECREATE")
tout = tin.CopyTree('0')
tout.Branch('BDT', BDT,'BDT/F')

N = tin.GetEntries()
     
for n in range(N):
    if n%1000 == 1:
        print "Applying:   "+str(n) +' of '+str(N) +' events evaluated.'
    tin.GetEntry(n)

    for varname in Var_names: Vars[varname][0] = getattr(tin,varname)
    
    BDT[0] = reader.EvaluateMVA('BDT')
    tout.Fill()
     
fout.Write("", TObject.kOverwrite)
fout.Close()
