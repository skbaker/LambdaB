# Plot the discriminating variables between signal and background.
#
# Use signal MC, and compare to sidebands of data, to represent background
#
# Choose variables based on ANA-NOTE of Lb -> LcPi paper 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from __future__ import division
import re

import sys
#print sys.argv[0] # prints python script
#print sys.argv[1] # prints command line argument

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

## Get colours from hex codes for signal and background
purple = TColor.GetColor("#5e3c99")
orange = TColor.GetColor("#e66101")


## Path to write plots to
PlotsPath = '/afs/cern.ch/user/s/skbaker/LambdaB/Normalisation/Plots/'

## Run ROOT in batch mode
gROOT.SetBatch(True)

## List of trigger branches to use
names = [ 'Lambda_b_PT'
        , 'Lambda_b_IPCHI2_OWNPV'
        , 'Lambda_b_OWNPV_CHI2'
        , 'Lambda_b_ENDVERTEX_CHI2'
        , 'Lambda_b_ETA'
        , 'Lambda_b_TAU'
        , 'Lambda_b_TAUCHI2'
        , 'Lambda_b_ENDVERTEX_X'
        , 'Lambda_b_ENDVERTEX_Y'
        , 'Lambda_b_ENDVERTEX_Z'
        #, 'Lambda_b_OWNPV_X' ## Different in MC/Data
        #, 'Lambda_b_OWNPV_Y'
        #, 'Lambda_b_OWNPV_Z'
        #, 'Lambda_b_OWNPV_NDOF'
        #, 'Lambda_b_OWNPV_COV_'
        , 'Lambda_b_IP_OWNPV'
        , 'Lambda_b_FD_OWNPV'
        , 'Lambda_b_FDCHI2_OWNPV'
        , 'Lambda_b_DIRA_OWNPV'
        #, 'Lambda_b_P' ## Don't want to use variables related to mass
        #, 'Lambda_b_PE'
        #, 'Lambda_b_PX'
        #, 'Lambda_b_PY'
        #, 'Lambda_b_PZ'
        #, 'Lambda_b_REFPX'
        #, 'Lambda_b_REFPY'
        #, 'Lambda_b_REFPZ'
        ]
print names

## Read in ROOT nTuple from command line
## Signal MC
SigMCTupleFileName = sys.argv[1]

sig_MC = TFile(SigMCTupleFileName)
sig = sig_MC.Get("Lb_Lcpi_Tuple/DecayTree")
sig.SetBranchStatus("*",0)
for name in names:
    sig.SetBranchStatus(name,1)

## Background data
BkgDataTupleFileName = sys.argv[2]

bkgData = TFile(BkgDataTupleFileName)
bkg = bkgData.Get("Lb_Lcpi_Tuple/DecayTree")
bkg.SetBranchStatus("*",0)
## Variables used that are not discriminants
bkg.SetBranchStatus("Lambda_b_M",1)
bkg.SetBranchStatus("Polarity",1)
for name in names:
    bkg.SetBranchStatus(name,1)

## Use to create plot name, and find polarity
Pieces = SigMCTupleFileName.split("/")
FrameFileName = Pieces[(len(Pieces)-1)]

## Cuts to select polarity and upper mass sideband
## Find polarity from root file name
if re.search('MagUp',FrameFileName): 
    pole = 1 
if re.search('MagDown',FrameFileName):
    pole = -1
bkgCut = "Lambda_b_M > 5900 && Lambda_b_M < 7000 && Polarity =="+str(pole)

## Total entries in tree
sig_entries = sig.GetEntries()
bkg_entries = bkg.GetEntries(bkgCut)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Loop through variables and plot each for sig and bkg
for i, name in enumerate(names):
    ## Create plot name
    ThisFrameFileName = FrameFileName.replace('.root','_'+name+'.png')
    FrameFilePath = PlotsPath+ThisFrameFileName

    ## Create histogram with extendable x-axis
    h_sig = TH1F("h_sig",name,500,0,0.1)
    ## X axis will extend to accommodate variable
    h_sig.GetXaxis().SetCanExtend(True)
    ## Set the axis for some of the variables
    if(name == 'Lambda_b_DIRA_OWNPV'):
        # Transforming variables could lead to more separation?
        #name = '(1-(1-Lambda_b_DIRA_OWNPV)^(1/4))'
        h_sig.GetXaxis().Set(500,0.999,1.0)
        h_sig.GetXaxis().SetCanExtend(False)
    if(name == 'Lambda_b_TAU'):
        h_sig.GetXaxis().Set(500,0.0,0.015)
        h_sig.GetXaxis().SetCanExtend(False)
    if(name == 'Lambda_b_FDCHI2_OWNPV'):
        h_sig.GetXaxis().Set(500,0.0,10000)
        h_sig.GetXaxis().SetCanExtend(False)
    if(name == 'Lambda_b_FD_OWNPV'):
        h_sig.GetXaxis().Set(500,0.0,200)
        h_sig.GetXaxis().SetCanExtend(False)
    h_sig.SetFillColor(purple)
    h_sig.SetLineColor(purple)
    h_sig.SetFillStyle(3008)
    h_sig.GetXaxis().SetTitle(name)

    ## Draw from tree to histogram
    cv = TCanvas()
    sigVariableToDraw = name+">>h_sig"
    bkgVariableToDraw = name+">>h_bkg"
    
    sig.Draw(sigVariableToDraw)
    
    ## Create histogram with extendable x-axis
    h_bkg = TH1F("h_bkg",name,500,h_sig.GetXaxis().GetXmin(),
                                  h_sig.GetXaxis().GetXmax())
    h_bkg.SetFillColor(orange)
    h_bkg.SetLineColor(orange)
    h_bkg.SetFillStyle(3020)
    bkg.Draw(bkgVariableToDraw, bkgCut)
    
    ## Normalise both plots to unity
    h_sig.Scale(1/sig_entries)
    h_bkg.Scale(1/bkg_entries)
 
    h_sig.Draw()
    h_bkg.Draw("same")
   
    leg = TLegend(0.65,0.8,0.85,0.9)
    leg.AddEntry(h_sig,"Signal MC","f")
    leg.AddEntry(h_bkg,"Background","f")
    leg.SetFillStyle(0)
    leg.SetLineWidth(0)
    leg.Draw()

    cv.SetBottomMargin(0.15)
    cv.SetRightMargin(0.15)
    cv.SaveAs(FrameFilePath)
    
    del h_sig
    del h_bkg

    ## Wait for command line input
    #raw_input("Waiting for user")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
