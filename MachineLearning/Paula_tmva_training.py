from ROOT import *

### Creating training samples
##############################

# f_sig = TFile("/vols/lhcb/palvare1/B2pphgamma/B2ppKgamma_MC/B2ppKgamma_S21.root")
# t_sig_all = f_sig.Get("B2pphgamma_Tuple/DecayTree")

# f_bkg = TFile("/vols/lhcb/palvare1/B2pphgamma/B2ppKgamma/B2pphgamma_S21_protonID.root")
# t_bkg_all = f_bkg.Get("B2pphgamma_Tuple/DecayTree")

# fout = TFile('/vols/lhcb/palvare1/B2pphgamma/BDT/training_samples.root','recreate')
# t_sig = t_sig_all.CopyTree("B_plus_BKGCAT<55") ## Truthmatching of the MC
# t_sig.Write("SignalTree")
# t_bkg = t_bkg_all.CopyTree("B_plus_MM>6000 && B_plus_MM<7000") ## Higher mass sideband 
# t_bkg.Write("BkgTree")

### Defining TMVA factory
##############################

fin = TFile('/vols/lhcb/palvare1/B2pphgamma/BDT/training_samples.root')
t_sig = fin.Get("SignalTree") 
t_bkg = fin.Get("BkgTree")

fout = TFile("training.root","recreate")
TMVA.Tools.Instance()

factory = TMVA.Factory('MyClassification', fout,
                            ":".join([
                                    "!V",
                                    "!Silent",
                                    "Color",
                                    "DrawProgressBar",
                                    "Transformations=I;D;P;G,D",
                                    "AnalysisType=Classification"])
                            )

factory.AddVariable("B_plus_ENDVERTEX_CHI2","F")
factory.AddVariable("B_plus_FDCHI2_OWNPV","F")
factory.AddVariable("B_plus_DIRA_OWNPV","F")

factory.AddSignalTree(t_sig)
factory.AddBackgroundTree(t_bkg)

# cuts
Cut = TCut("1") 

factory.PrepareTrainingAndTestTree(Cut,
                                   ":".join(["nTrain_Signal=0",
                                             "nTrain_Background=0",
                                             "SplitMode=Random",
                                             "NormMode=NumEvents",
                                             "!V"]))

## Defining the classifiers to train
method = factory.BookMethod(TMVA.Types.kBDT, "BDT",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=850",
                                      # "MinNodeSize=0.5%",
                                      "MaxDepth=3",
                                      "BoostType=AdaBoost",
                                      "AdaBoostBeta=0.5",
                                      "SeparationType=GiniIndex",
                                      "nCuts=20",
                                      "PruneMethod=NoPruning",
                                      ]))

### Traingin and testing
##############################
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()


### Visualising the results
##############################
# TMVA.TMVAGUI("training.root")
