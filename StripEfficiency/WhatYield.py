# Script to estimate the yield of signal events
#
# Equation found in LHCb-ANA-2012-064 [Eq. 1]
# N_sig = L_int . sigma_bb . 2 . f_Lambda . BR_vis . eff_tot
# where L_int = N_events / pp cross section

# Find ratio of N_sig / N_events:
sigma_bb = 284E-6 # = 284 microbarn, bbbar production cross-section 
f_Lambda = 0.09
BR_vis = 1E-8
eff_tot = 0.01
L_int = 851056.87E9 # per barn -- stored lumi from RunDB for 1/8/16 to 26/10/16

N_sig = L_int*sigma_bb*2*f_Lambda*BR_vis*eff_tot

print N_sig
