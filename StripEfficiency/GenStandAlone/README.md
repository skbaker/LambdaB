## GenStandAlone

Get stats from generator level.  
i.e. Number of Lb-\>dp events vs. all generated events 

### GenStandAlone/Gauss-Job.py
Run this to find generator level stats.

### GenStandAlone/GeneratorLog.xml
Stats from Gauss generation of 15102021 events.  
