#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#
# Use to find the production efficiency

from Gauss.Configuration import *

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.RunNumber = 1082

#--Number of events
nEvts = 15
LHCbApp().EvtMax = nEvts

#--Instead of calling all options along with gaudirun.py
importOptions("$GAUSSOPTS/Gauss-2012.py")
importOptions("$GAUSSOPTS/GenStandAlone.py")
importOptions("$DECFILESROOT/options/15102021.py") # Lb -> dp 

