# Count the total number of events across the LFNs
# 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
print 'MagUp LFN EventCount:'
total_MU = 0

with open("MetaData_LFNs_MU.txt") as file:
    for line in file:
        if "EventStat" in line:
            if line:
                [int(s) for s in line.split() if s.isdigit()]
#                print s
                total_MU = total_MU + int(s)
print total_MU
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
print 'MagDown LFN EventCount:'
total_MD = 0

with open("MetaData_LFNs_MD.txt") as file:
    for line in file:
        if "EventStat" in line:
            if line:
                [int(s) for s in line.split() if s.isdigit()]
#                print s
                total_MD = total_MD + int(s)
print total_MD
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
