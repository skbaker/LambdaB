## Stripping efficiency

Need to find efficiency of stripping and trigger so that more MC can be ordered.  
Old MC was with 2012 conditions, so will not be of use for new data.

DecFile:   
15102021 (Lb -> dp)  
15104040 (Lb -> dppipi)  
`Lb_dp=DecProdCut.dec` and `Lb_dppipi=DecProdCut.dec`  
Phase space decay, decay products in LHCb acceptance.  

Stripping file at https://gitlab.cern.ch/lhcb/Stripping/blob/640c3635d354b8332a126ca35030abde319ec662/Phys/StrippingArchive/python/StrippingArchive/Stripping26/StrippingQEE/StrippingLb2dp.py   

### MetaData\_LFNs\_M\*.txt
Contain meta data of the xdigi files.   
Found using command `dirac-bookkeeping-file-metadata --File==LFNs_M*.dat --Full`.  
Use these to extract the number of events before stripping and trigger.  

### CountEvents.py
Script to collect EventStats from `MetaData_LFNs_M*.txt` files.  
These should be piped into the next script.  
Is it possible to automate the `dirac-bookkeeping-file-metadata` command too?  

### GetStripEfficiency.C
Script to compare number of Lambda\_b before and after trigger categories.  
Triggers used:
* Lambda\_b\_L0Global\_Dec
* Lambda\_b\_Hlt1Phys\_Dec
* Lambda\_b\_Hlt2Phys\_Dec   


Resulting efficiency should be trigger/stripping

### WhatYield.py
Quick script to estimate the Lambda\_b yield
