### MC processing options for DaVinci for Lb -> dppipi

* `CutOnMCTruth.py` can be used to remove and duplicates in the nTuple by cutting
and selecting based on MCTruth ID information  

Run DaVinci script over the DSTs on GRID using Ganga.   
Simply run Ganga script `ganga ganga_MC_DaVinci.py`.  
Have to download root nTuples from Ganga afterwards. 
Find these at `/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/`.
