# Copy MC nTuples, such that only the true particles are selected
# i.e. that protons are protons, and their mother is the Lb
#
# If PyROOT will not run, go into an LHCb environment to set it up
# e.g. $ SetupProject Urania v5r0
#
# Run with magnet orientation argument: MagUp = mu, MagDown = md
# i.e. $ python CutOnMCTruth.py mu
#
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Command line argument input
import sys
#print sys.argv[0] # prints CutOnMCTruth.py
#print sys.argv[1] # prints argument

from ROOT import *

# Run ROOT in batch mode
gROOT.SetBatch(True)

# Tuple covering full mass range
FullTupleFile = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/'\
                'MC_Lb_dppipi_2016_{}.root'.format(sys.argv[1])
print FullTupleFile

original = TFile(FullTupleFile)
oldTree = original.Get('Lb_dppipi_Tuple/DecayTree')

# New file for cut tree
ClonedTupleFile = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/'\
                  'Pure_MC_Lb_dppipi_2016_{}.root'.format(sys.argv[1])

cloned = TFile(ClonedTupleFile,'recreate')
newTree = oldTree.CopyTree('abs(p_TRUEID) == 2212'
                       ' && abs(p_MC_MOTHER_ID) == 5122'
                       ' && abs(deut_TRUEID) == 1000010020'
                       ' && abs(deut_MC_MOTHER_ID) == 5122'
                       ' && abs(pi_plus_TRUEID) == 211 '
                       ' && abs(pi_plus_MC_MOTHER_ID) == 5122'
                       ' && abs(pi_minus_TRUEID) == 211 '
                       ' && abs(pi_minus_MC_MOTHER_ID) == 5122')

print 'Before cut'
print oldTree.GetEntriesFast()
print 'After cut'
print newTree.GetEntriesFast()

cloned.Write()
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
