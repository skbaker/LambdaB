#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Run DaVinci on the grid
## Script used to process DST files for 2016 MC into nTuples.
## Run a separate Ganga job for each collection of DSTs: MagUp/MagDown,
## ROOT nTuples must be merged afterwards
##
## Name of ROOT nTuples needs to match name in DaVinci script
##
## Run as: $ ganga ganga_DaVinci.py
## 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Sumbmission functions copied from Matt:
def sendJob(filename, dataset, name):
    j = Job()
    j.application = DaVinci(version='v41r1')
    j.application.optsfile = [ filename ]
    j.backend = Dirac()
    j.inputdata = dataset
    j.splitter = SplitByFiles(filesPerJob=3)
    j.outputfiles = [ DiracFile('MC_Lb_dppipi_2016.root') ]
    j.parallel_submit = True
    j.name = name
    j.submit()

def getFromBookkeeping(datapath):
    bkk = BKQuery(datapath)
    data = bkk.getDataset()
    return data

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Try submitting job using Matt's method
firstDataSetPath = '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/15104040/ALLSTREAMS.DST'
#firstDataSetPath = '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/15104040/ALLSTREAMS.DST'

Data = getFromBookkeeping(firstDataSetPath)

## Modify so that Data is only one file, for trial
## REMOVE this line to submit the full dataset
#Data.files = Data.files[0:1]
#print Data

sendJob('DaVinci_Lb_dppipi_MC_selection_2016.py', Data, firstDataSetPath)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
