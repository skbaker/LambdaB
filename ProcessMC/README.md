## MC processing options for Brunel and DaVinci

Directory for processing DST files of MC data for analysis

### Newer scripts for 2016 MC, using Ganga
* `ganga_MC_DaVinci.py`: options script for Ganga submission. DaVinci version
here matches the local cmtuser one, with DLLd
* `DaVinci_Lb_dp_MC_selection_2016.py`: options file to select Lb -> dp 
candidates from 2016 MC simulation
