### Fit to signal, background and combinatorial
### Should do this after following the selection first -- make new data nTuple
### for the data surviving the selection?
##
## Run as `python Fits.py quick/slow` -- choose last option
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import sys

runOption = sys.argv[1]
if(runOption != 'quick' and runOption != 'slow'): 
    print 'Please run again, selecting execution type <python Fits.py ____>'
    print 'Option should either be <slow>, for full fit, or <quick>'
    sys.exit()
if(runOption == 'quick'): print 'Running without fitting to MC and data'
if(runOption == 'slow'):  print 'Running with full fit and save'

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

purple = TColor.GetColor('#5e3c99')
orange = TColor.GetColor('#e66101')
lightblue = TColor.GetColor('#a6cee3')
blue = TColor.GetColor('#1f78b4')
lightgreen = TColor.GetColor('#b2d8a')
pink = TColor.GetColor('#fb9a99')

#gROOT.SetBatch(True)
## Use Argus(X)Gauss, instead of RooPhysBkg
#gROOT.ProcessLine(".x RooPhysBkg.cxx++")

PlotsPath = '/afs/cern.ch/user/s/skbaker/LambdaB/Normalisation/Plots/'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Load data sets
## MC
SignalFile = TFile('/eos/lhcb/user/s/skbaker/Lb_Lcpi/MC/Tuples/2016/'\
                   'Selected_ANA2016_MC_Lb_LcPi_2016.root')
                   #'Pure_MC_Lb_LcPi_2016_MagDown.root')
t_sig = SignalFile.Get("DecayTree")
print t_sig
t_sig.SetBranchStatus("*",0)
t_sig.SetBranchStatus("Lambda_b_M",1)

## Data
DataFile = TFile('/eos/lhcb/user/s/skbaker/Lb_Lcpi/Data/Tuples/'\
                 'Selected_ANA2016_Data_Lb_LcPi_2016.root')
                 #'Selected_Data_Lb_LcPi_2016.root')
t_data = DataFile.Get("DecayTree")
t_data.SetBranchStatus("*",0)
t_data.SetBranchStatus("Lambda_b_M",1)
## Make selection on LambdaC mass? Should put this in before making pure tuple?

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Mass window and sideband limits
low_LHS = 5350
low_RHS = 5770
top_RHS = 6000
low_window = 5470
at_mass = 5620
threshold = 5620.2 - 134.977

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Fit
## Observable
M_Lb = RooRealVar('Lambda_b_M','M_{#Lambda_{b}} [MeV]',low_LHS,top_RHS)

M_Lb.setRange('all',low_LHS,top_RHS)
M_Lb.setRange('signal',low_window,low_RHS)
M_Lb.setRange('sideband',low_RHS,top_RHS)
M_Lb.setRange('LHSideband',low_LHS,at_mass)

## ~~~ SIGNAL ~~~
mean = RooRealVar('mean','M_{mean}',5620.2,5600.0,5650.0)
width = RooRealVar('width','M_{width}',13.0,5.0,25.0)

## Crystal ball functions
alpha1 = RooRealVar('alpha1','#alpha_{1}',0.0,10.0)
n1 = RooRealVar('n1','n1',0.0,10.0)
CB1 = RooCBShape('CB1','Crystal Ball 1',M_Lb,mean,width,alpha1,n1)

alpha2 = RooRealVar('alpha2','#alpha_{2}',-10.0,0.0)
n2 = RooRealVar('n2','n2',0.0,10.0)
CB2 = RooCBShape('CB2','Crystal Ball 2',M_Lb,mean,width,alpha2,n2)

## ~~~ COMBINATORIAL ~~~ 
## Exponential function
expconst = RooRealVar('expconst','expconst',-0.002,-0.01,0)
exp = RooExponential('exp','Comb. Bkg.',M_Lb,expconst)

## ~~~ PART RECO ~~~
## Use Argus convolved with Gauss as in LHCb-ANA-2016-030
mt = RooRealVar('mt','Threshold M',threshold,(threshold-20.0),(threshold+20.0))
c = RooRealVar('c','Slope',-1.0,-25.0,0.0)
p = RooRealVar('p','Curvature',0.5,0.0,1.0)
argus = RooArgusBG('argus','Argus',M_Lb,mt,c,p)

centre = RooRealVar('centre','Gauss mean',0)
gauss = RooGaussian('gauss','Gauss',M_Lb,centre,width)
## Try isolating the Gauss width to investigate rising edge on RHS of A(X)G
## -- Using separate widths does not solve rising edge issue.
## -- Is it something to do with the fit range instead?
#PRwidth = RooRealVar('PRwidth','Gauss width',15.0,10.0,20.0)
#gauss = RooGaussian('gauss','Gauss',M_Lb,centre,PRwidth)

## Convolute
shoulder = RooFFTConvPdf('shoulder','ArgusXGauss for part-reco',M_Lb,\
                          argus,gauss)

## RooPhysBkg is used in other analyses - step conv. with Gaussian
## Can't get this function to work -- abandon, but leave here for posterity
#s = RooRealVar('s','Slope',1.0,-200.0,200.0)
#stepbkg = RooPhysBkg('stepbkg','Gauss(X)step',M_Lb,mt,s,PRwidth)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Make RooData
sig = RooDataSet('sig','sig',t_sig,RooArgSet(M_Lb))
data = RooDataSet('data','data',t_data,RooArgSet(M_Lb))

nsig  = RooRealVar("nsig","signal fraction",5000,0,10000000)
npart = RooRealVar("npart","part reco background",5000,0,10000000)
ncomb = RooRealVar("ncomb","combinatorial background",5000,0,10000000)
fraction = RooRealVar("fraction","fraction between CB1 and CB2",0.5)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Double crystal ball
double = RooAddPdf("double","Double CB",RooArgList(CB1,CB2),\
                  RooArgList(fraction,fraction))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Fit model to data following Thibaud's method:
## Find his method at https://gitlab/cern.ch/thumair/SuperLenin/blob/master/
## src/fitAndSplotKmumuDataDTFForTraining.cpp

if(runOption=='slow'): double.fitTo(sig,RooFit.Range('all'),RooFit.Save())

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Plot MC fit
cv = TCanvas()
cv.SetRightMargin(0.05)
cv.SetLeftMargin(0.11)
cv = TCanvas('cv','cv',700,700)
cv.Divide(1,2)

cv.cd(1)
gPad.SetPad(0.0,0.2,1.0,1.0)
gPad.SetRightMargin(0.05)
gPad.SetLeftMargin(0.13)

frame = M_Lb.frame()
frame.GetYaxis().SetTitleOffset(1.3)
frame.GetXaxis().SetTitleOffset(1.1)

sig.plotOn(frame)
double.plotOn(frame,RooFit.LineColor(lightblue),RooFit.LineStyle(kDashed))

frame.Draw()

## Plot pulls on lower pad of canvas
cv.cd(2)
gPad.SetPad(0.0,0.0,1.0,0.2)
gPad.SetRightMargin(0.05)
gPad.SetLeftMargin(0.13)

pullframe = M_Lb.frame()
pull = RooHist(frame.pullHist())
pullframe.GetYaxis().SetLabelSize(0)
pullframe.GetXaxis().SetLabelSize(0)
pullframe.GetXaxis().SetTitle('')

pullframe.addPlotable(pull,'P')
pullframe.Draw()

## Draw line through middle of pull plot
midLine = TLine(low_LHS,0.0,top_RHS,0.0)
midLine.Draw()

if(runOption=='slow'): cv.SaveAs(PlotsPath+'MCFit.png')

raw_input("Press ENTER to continue")

del cv
del frame
del pullframe
del midLine
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Fix doubleCB parameters to MC fit results
alpha1.setConstant(True)
n1.setConstant(True)
alpha2.setConstant(True)
n2.setConstant(True)
## Leave mean and width free, as in LHCb-ANA-2013-023

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Combine functions and fit to data 
shapes = RooArgList()
shapes.add(double)
shapes.add(exp)
shapes.add(shoulder)

yields = RooArgList()
yields.add(nsig)
yields.add(ncomb)
yields.add(npart)

combined = RooAddPdf("combined","double CB + exp + Argus",shapes,yields)

if(runOption=='slow'): combined.fitTo(data, RooFit.Range('all'),RooFit.Save())
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Plot the total fit and the pulls
cv = TCanvas('cv','cv',700,700)
cv.Divide(1,2)

cv.cd(1)
gPad.SetPad(0.0,0.2,1.0,1.0)
gPad.SetRightMargin(0.05)
gPad.SetLeftMargin(0.13)
gPad.SetLogy()

fullframe = M_Lb.frame()
fullframe.GetYaxis().SetTitleOffset(1.6)
fullframe.GetXaxis().SetTitleOffset(1.1)

data.plotOn(fullframe)
combined.plotOn(fullframe,RooFit.Components('shoulder'),RooFit.LineColor(pink),\
                      RooFit.LineStyle(kDashed))
combined.plotOn(fullframe,RooFit.Components('exp'),RooFit.LineColor(lightgreen),\
                      RooFit.LineStyle(kDashed))
combined.plotOn(fullframe,RooFit.Components('double'),RooFit.LineColor(lightblue),\
                      RooFit.LineStyle(kDashed))
combined.plotOn(fullframe,RooFit.LineColor(blue))

fullframe.Draw()

print fullframe.chiSquare()

## Plot pulls on lower pad of canvas
cv.cd(2)
gPad.SetPad(0.0,0.0,1.0,0.2)
gPad.SetRightMargin(0.05)
gPad.SetLeftMargin(0.13)

pullframe = M_Lb.frame()
pull = RooHist(fullframe.pullHist())
pullframe.GetYaxis().SetLabelSize(0)
pullframe.GetXaxis().SetLabelSize(0)
pullframe.GetXaxis().SetTitle('')

pullframe.addPlotable(pull,'P')
pullframe.Draw()

## Draw line through middle of pull plot
midLine = TLine(low_LHS,0.0,top_RHS,0.0)
midLine.Draw()

#if(runOption=='slow'): cv.SaveAs(PlotsPath+'Fits.png')
if(runOption=='slow'): cv.SaveAs(PlotsPath+'Fits_logScale.png')

ResultsFile = TFile('FitResult.root','recreate')
combined.Write()
double.Write()
ResultsFile.Close()

raw_input("Press ENTER to continue")

del cv
del fullframe
del pullframe

## What is the rising edge on the right of the Argus(X)Gauss??
## It disappears if the width of the Gaussian is decreased

## Set parameters as constants to plot this separately:
centre.setConstant(True)
width.setConstant(True)
c.setConstant(True)
p.setConstant(True)
mt.setConstant(True)

centre.Print()
width.Print()
c.Print()
p.Print()
mt.Print()

## Want to see plot over bigger range
M_Lb.setMin(5300)
M_Lb.setMax(6100)

cv = TCanvas()
cv.SetRightMargin(0.05)
cv.SetLeftMargin(0.11)
cv.cd()
cv.SetLogy()
argusframe = M_Lb.frame()
argusframe.GetYaxis().SetTitleOffset(1.3)
argusframe.GetXaxis().SetTitleOffset(1.1)
shoulder.plotOn(argusframe,RooFit.LineColor(pink),RooFit.LineStyle(kDashed))
argusframe.Draw()
if(runOption=='slow'): cv.SaveAs(PlotsPath+'Fits_ArgusXGauss.png')

raw_input("Press ENTER to continue")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
