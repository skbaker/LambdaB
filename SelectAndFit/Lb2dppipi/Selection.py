#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Follow the same selection as for the normalisation channel, but with 
## different PID requirements.
## Also need to veto the Lambda_C mass
##
## (Follow LHCb-ANA-2013-023)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from __future__ import division
import re
import string
import sys
#print sys.argv[1] # argument with python command

from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

purple = TColor.GetColor('#5e3c99')
orange = TColor.GetColor('#e66101')

## Path to write plots too
PlotsPath = '/afs/cern.ch/user/s/skbaker/LambdaB/SelectAndFit/Lb2dppipi/Plots/'

## Run ROOT in batch mode
#gROOT.SetBatch(True)

## List of branches to use
names = [ 'Lambda_b_M_deuthyp'
        , 'Lambda_b_IPCHI2_OWNPV'
        , 'Lambda_b_L0HadronDecision_TOS'
        , 'Lambda_b_L0Global_TIS'
        , 'Lambda_b_L0Global_Dec'
        , 'Lambda_b_Hlt1Global_Dec'
        , 'Lambda_b_Hlt2Global_Dec'
        , 'Lambda_b_Hlt1Phys_TOS'
        , 'Lambda_b_Hlt2Phys_TOS'
#        , 'deut_PIDd'
        , 'deut_PIDp'
        , 'p_PIDp'
        , 'p_PIDK'
        ## Additional variables for BDT:
        , 'Lambda_b_OWNPV_CHI2'
        , 'Lambda_b_DIRA_OWNPV'
        , 'Lambda_b_TAU'
        , 'Lambda_b_TAUCHI2'
        , 'Lambda_b_FDCHI2_OWNPV'
        , 'Lambda_b_ENDVERTEX_CHI2'
        , 'p_TRACK_CHI2NDOF'
        , 'p_IPCHI2_OWNPV'
        , 'deut_TRACK_CHI2NDOF'
        , 'deut_IPCHI2_OWNPV'
        , 'Polarity'
        ## PID variables for LHCb-ANA-2016-030 selection
        ## If following same selection as normalisation channel
        , 'p_ProbNNp'
        , 'p_ProbNNk'
        , 'p_ProbNNpi'
        , 'deut_ProbNNpi'
        , 'deut_ProbNNk'
        , 'deut_ProbNNp'
        , 'pi_plus_PIDp'
        , 'pi_minus_PIDp'
        , 'pi_plus_ProbNNpi'
        , 'pi_minus_ProbNNpi'
        ## Variables used for kinematic Lc(pKpi) cut
        , 'p_ID'
        , 'pi_plus_ID'
        , 'pi_minus_ID'
        , 'deut_ID'
        , 'p_P'
        , 'pi_plus_P'
        , 'pi_minus_P'
        , 'deut_P'
        , 'p_PX', 'p_PY', 'p_PZ'
        , 'pi_plus_PX', 'pi_plus_PY', 'pi_plus_PZ'
        , 'pi_minus_PX', 'pi_minus_PY', 'pi_minus_PZ'
        , 'deut_PX', 'deut_PY', 'deut_PZ'
       ]

print names

## Read in nTuples
DataFileName = '/eos/lhcb/user/s/skbaker/Lb_dppipi_Strip26/Data/'\
               'BLIND_Lb_dppipi_S26_2016_md.root' 
               #'BLIND_Lb_dppipi_S26_2016_mu.root' 
## 2016
MCFileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/'\
             'Pure_MC_Lb_dppipi_2016_MagDown.root'
             #'Pure_MC_Lb_dppipi_2016_MagUp.root'

MCfile = TFile(MCFileName)
MC = MCfile.Get("DecayTree")
MC.SetBranchStatus("*",0)
for name in names:
    MC.SetBranchStatus(name,1)

Datafile = TFile(DataFileName)
Data = Datafile.Get("DecayTree")
Data.SetBranchStatus("*",0)
for name in names:
    Data.SetBranchStatus(name,1)

## Print entries in trees
print Data.GetEntries()
print MC.GetEntries()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Selection criteria - from LHCb-ANA-2013-023
## Triggers are different for signal to normalisation channel. Less efficient
## signal, so use differently?

#L0Trigger = '(Lambda_b_L0HadronDecision_TOS == 1 || Lambda_b_L0Global_TIS == 1)'
L0Trigger = '(Lambda_b_L0HadronDecision_TOS == 1 || Lambda_b_L0Global_Dec == 1)'
#HLT = '(Lambda_b_Hlt1Phys_TOS == 1 && Lambda_b_Hlt2Phys_TOS == 1)'
HLT = '(Lambda_b_Hlt1Global_Dec == 1 || Lambda_b_Hlt2Global_Dec == 1)'
#Stripping = Already done
#BDTG > 0.66 # Need to run the BDT first...
BeautyMass = '(Lambda_b_M_deuthyp > 5350 && Lambda_b_M_deuthyp < 6000)'
## deut_PIDd is NOT in the data
DeutPID = 'deut_PIDd > 5'
#ProtonPID = '(p_PIDp > 5 && (p_PIDp - p_PIDK) > 0)'
ProtonPID = 'p_ProbNNp > 0.5'
PionPID = 'pi_plus_ProbNNpi > 0.4 && pi_minus_ProbNNpi > 0.4'

## PID selection from LHCb-ANA-2016-030
## This was for pion <--> kaon separation, so don't need it?
BaseCut = 0.15
NewCut = 0.25
MoreCut = 0.35
Cut = BaseCut

DeutPID2016 = '((deut_ProbNNpi - 1)*(deut_ProbNNpi - 1) + '\
              '(deut_ProbNNk)*(deut_ProbNNk)) >'+str(Cut)
ProtonPID2016 = 'p_ProbNNp > 0.5'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Make kinematic cuts to remove Lc(pKpi), where deuteron has K mass
## Particle masses:
M_deut = 1875.613
M_prot =  938.272
M_kaon =  493.677
M_pion =  139.570

def Sub3MassesPlus():
    #M2 = E2 - P2
    SubString = 'sqrt(((sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_plus_P*pi_plus_P)) *'\
                '(sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_plus_P*pi_plus_P))) -'\
                '(((deut_PX + p_PX + pi_plus_PX)*'\
                '(deut_PX + p_PX + pi_plus_PX)) + '\
                '((deut_PY + p_PY + pi_plus_PY)*'\
                '(deut_PY + p_PY + pi_plus_PY)) + '\
                '((deut_PZ + p_PZ + pi_plus_PZ)*'\
                '(deut_PZ + p_PZ + pi_plus_PZ))))'
    NewString = string.replace(SubString, 'M_d', str(M_kaon))
    NewString = string.replace(NewString, 'M_P', str(M_prot))
    NewString = string.replace(NewString, 'M_pi', str(M_pion))
    return NewString


def Sub3MassesMinus():
    #M2 = E2 - P2
    SubString = 'sqrt(((sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_minus_P*pi_minus_P)) *'\
                '(sqrt(M_d*M_d + deut_P*deut_P) +'\
                'sqrt(M_P*M_P + p_P*p_P) + '\
                'sqrt(M_pi*M_pi + pi_minus_P*pi_minus_P))) -'\
                '(((deut_PX + p_PX + pi_minus_PX)*'\
                '(deut_PX + p_PX + pi_minus_PX)) + '\
                '((deut_PY + p_PY + pi_minus_PY)*'\
                '(deut_PY + p_PY + pi_minus_PY)) + '\
                '((deut_PZ + p_PZ + pi_minus_PZ)*'\
                '(deut_PZ + p_PZ + pi_minus_PZ))))'
    NewString = string.replace(SubString, 'M_d', str(M_kaon))
    NewString = string.replace(NewString, 'M_P', str(M_prot))
    NewString = string.replace(NewString, 'M_pi', str(M_pion))
    return NewString

CutLcMass = '(((p_ID > 0 && pi_plus_ID > 0) && ('+Sub3MassesPlus()+' > 2305 ||'\
            +Sub3MassesPlus()+'< 2265))'\
            '|| ((p_ID < 0 && pi_plus_ID < 0) && ('+Sub3MassesPlus()+' > 2305 ||'\
            +Sub3MassesPlus()+'< 2265))'\
            '|| ((p_ID > 0 && pi_minus_ID > 0) && ('+Sub3MassesMinus()+' > 2305 ||'\
            +Sub3MassesMinus()+'< 2265))'\
            '|| ((p_ID < 0 && pi_minus_ID < 0) && ('+Sub3MassesMinus()+' > 2305 ||'\
            +Sub3MassesMinus()+'< 2265)))' 

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Make cuts on the trees, following the AnaNote, and see what remains

## deut_PIDd is NOT in the data
#selection2013 = DeutPID + '&&' + ProtonPID\
selection2013 = ProtonPID + '&&' + PionPID\
            + '&&'  + BeautyMass\
            + '&&' + L0Trigger + '&&' + HLT\
            + '&&' + CutLcMass
print selection2013 

selection2016 = DeutPID2016 + '&&' + ProtonPID2016\
            + '&&' + BeautyMass\
            + '&&' + L0Trigger + '&&' + HLT 
print selection2016

## Choose 2013 or 2016 ANA note selections
selection = selection2013
print selection

cv = TCanvas()
cv.SetTicks()
cv.SetRightMargin(0.05)
cv.SetLeftMargin(0.11)
h1 = TH1F('h1','h1',300,5000,6500)
h1.SetFillColor(orange)
h1.SetLineColor(orange)
h1.SetFillStyle(3020)
h1.GetXaxis().SetTitle('M_{#Lambda_{b}} [MeV/c^{2}]')
h1.GetYaxis().SetTitle('Candidates /(5 MeV/c^{2})')
h1.GetXaxis().SetTitleOffset(1.25)
h1.GetYaxis().SetTitleOffset(1.45)
Data.Draw('Lambda_b_M_deuthyp>>h1')

h2 = TH1F('h2','h2',300,5000,6500)
h2.SetFillColor(purple)
h2.SetLineColor(purple)
h2.SetFillStyle(3008)
Data.Draw('Lambda_b_M_deuthyp>>h2',selection)

h1.Draw()
h2.Draw('same')

cv.SaveAs(PlotsPath+'Selection_Data_Lb_dppipi.png')
"""
## Make new nTuple with this selected data
NewFileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip26/Data/'\
              'Selected_Data_Lb_dp_2016.root'

SelectFile = TFile(NewFileName,'recreate')
## Only the active branches are copied here, so not the full tree!
newTree = Data.CopyTree(selection)
print Data.GetEntriesFast()
print newTree.GetEntriesFast
#SelectFile.Write()
"""
raw_input("Press ENTER to continue")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Make same selection on truth-matched signal MC, to find selection efficiency 

c1 = TCanvas()
c1.SetTicks()
c1.SetRightMargin(0.05)
c1.SetLeftMargin(0.11)
h3 = TH1F('h3','h3',300,5000,6500)
h3.SetFillColor(orange)
h3.SetLineColor(orange)
h3.SetFillStyle(3020)
h3.GetXaxis().SetTitle('M_{#Lambda_{b}} [MeV/c^{2}]')
h3.GetYaxis().SetTitle('Candidates /(5 MeV/c^{2})')
h3.GetXaxis().SetTitleOffset(1.25)
h3.GetYaxis().SetTitleOffset(1.45)
MC.Draw('Lambda_b_M_deuthyp>>h3')

h4 = TH1F('h4','h4',300,5000,6500)
h4.SetFillColor(purple)
h4.SetLineColor(purple)
h4.SetFillStyle(3008)
MC.Draw('Lambda_b_M_deuthyp>>h4',selection)

h3.Draw()
h4.Draw("same")

leg2 = TLegend(0.15,0.78,0.45,0.85)
leg2.SetFillStyle(0)
leg2.SetLineWidth(0)
leg2.SetHeader('Cut eff: '+('{:01.3f}'.\
                       format((MC.GetEntries(selection)/MC.GetEntries()))))
leg2.Draw()

c1.SaveAs(PlotsPath+'Selection_MC_Lb_dppipi_2016.png')

## Make new nTuple with this selected MC 
NewMCFileName = '/eos/lhcb/user/s/skbaker/Lb_dp_Strip28/MC/Tuples/Lb_dppipi/'\
                'Selected_MC_Lb_dppipi_2016.root'
SelectMCFile = TFile(NewMCFileName,'recreate')
"""
## Write selected pure MC to file too, so fit to this, instead of full pure MC
newMCTree = MC.CopyTree(selection)
print MC.GetEntriesFast()
print newMCTree.GetEntriesFast
#SelectMCFile.Write()
"""
raw_input("Press ENTER to continue")

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
