# Select and fit

Scripts to run signal selection and fitting code.  
Unlike normalisation channel, the signal channel is blind, so fitting strategy
may be slightly different.  

No expected peaking backgrounds in signal.

### Selection.py
Script to select signal.  
Once happy with selection, need to `Write()` the selection to ROOT files -- 
change this in the script!


The selection for signal is pretty different to normalisation channel.
Can something be done to improve efficiency? Should just rely on BDT instead?


### Fits.py
Script copied from normalisation channel. However, models will be different
because no peaking backgrounds expected.
